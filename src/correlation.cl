
// sampler must be set to use normalized coordinates
__kernel void correlate2d(__read_only image2d_t src, sampler_t const sampler,
    __global float* filter, int2 const filter_width, uint4 const filter_mask,
    __write_only image2d_t dst) {
    int2 const xy_dst = (int2)(get_global_id(0), get_global_id(1));
    if (xy_dst.x >= get_image_width(dst) || xy_dst.y >= get_image_height(dst)) return;

    float2 const half_width = (float2)(filter_width.x, filter_width.y)/(float2)2;
    float2 const src_dim = (float2)(get_image_width(src), get_image_height(src));
    float2 const src_tl = (float2)(xy_dst.x, xy_dst.y)-floor(half_width);
    float4 sum = 0;

    float4 const src_px = read_imagef(src, sampler, ((src_tl+floor(half_width))/src_dim));

    for (int r=0; r<filter_width.y; ++r) {
        for (int c=0; c<filter_width.x; ++c) {
            float4 const weight = filter[r*filter_width.x+c];
            float2 const xy_off = (float2)(r,c);
            float2 const xy_src = (src_tl+xy_off)/src_dim;
            sum += weight*read_imagef(src, sampler, xy_src);
        }
    }

    float4 const dst_px = select(src_px, sum, filter_mask);
    write_imagef(dst, xy_dst, dst_px);
}

// sampler must be set to use normalized coordinates
__kernel void correlate1d_x(__read_only image2d_t src, sampler_t const sampler,
    __global float* filter, int const filter_width, uint4 const filter_mask,
    __write_only image2d_t dst) {
    int2 const xy_dst = (int2)(get_global_id(0), get_global_id(1));
    if (xy_dst.x >= get_image_width(dst) || xy_dst.y >= get_image_height(dst)) return;

    float2 const half_width = (float2)(filter_width/2, 0);
    float2 const src_dim = (float2)(get_image_width(src), get_image_height(src));
    float2 const src_tl = (float2)(xy_dst.x, xy_dst.y)-floor(half_width);
    float4 sum = 0;

    float4 const src_px = read_imagef(src, sampler, ((src_tl+floor(half_width))/src_dim));

    for (int i=0; i<filter_width; ++i) {
        float4 const weight = filter[i];
        float2 const xy_off = (float2)(i,0);
        float2 const xy_src = (src_tl+xy_off)/src_dim;
        sum += weight*read_imagef(src, sampler, xy_src);
    }

    float4 const dst_px = select(src_px, sum, filter_mask);
    write_imagef(dst, xy_dst, dst_px);
}

// sampler must be set to use normalized coordinates
__kernel void correlate1d_y(__read_only image2d_t src, sampler_t const sampler,
    __global float* filter, int const filter_width, uint4 const filter_mask,
    __write_only image2d_t dst) {
    int2 const xy_dst = (int2)(get_global_id(0), get_global_id(1));
    if (xy_dst.x >= get_image_width(dst) || xy_dst.y >= get_image_height(dst)) return;

    float2 const half_width = (float2)(0, filter_width/2);
    float2 const src_dim = (float2)(get_image_width(src), get_image_height(src));
    float2 const src_tl = (float2)(xy_dst.x, xy_dst.y)-floor(half_width);
    float4 sum = 0;

    float4 const src_px = read_imagef(src, sampler, ((src_tl+floor(half_width))/src_dim));

    for (int i=0; i<filter_width; ++i) {
        float4 const weight = filter[i];
        float2 const xy_off = (float2)(0,i);
        float2 const xy_src = (src_tl+xy_off)/src_dim;
        sum += weight*read_imagef(src, sampler, xy_src);
    }

    float4 const dst_px = select(src_px, sum, filter_mask);
    write_imagef(dst, xy_dst, dst_px);
}

