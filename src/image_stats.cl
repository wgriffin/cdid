__constant sampler_t gSampler =
    CLK_NORMALIZED_COORDS_FALSE|CLK_ADDRESS_NONE|CLK_FILTER_NEAREST;

__kernel void compute_min_partial(__read_only image2d_t in, __local float4* partial,
                                  __write_only image2d_t out) {
    int2 const dim = (int2)(get_image_width(in), get_image_height(in));

    int2 const xy = (int2)(get_global_id(0), get_global_id(1));
    if (xy.x >= dim.x || xy.y >= dim.y) return;

    int2 const id = (int2)(get_local_id(0), get_local_id(1));
    if (id.x >= dim.x || id.y >= dim.y) return;

    int2 const sz = min(dim, (int2)(get_local_size(0), get_local_size(1)));
    int const offset = id.y*sz.x + id.x;

    partial[offset] = read_imagef(in, gSampler, xy);
    barrier(CLK_LOCAL_MEM_FENCE);

    for (int i=(sz.x*sz.y)/2; i>0; i>>=1) {
        if (offset < i) partial[offset] = min(partial[offset], partial[offset+i]);
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    if (id.x == 0 && id.y == 0) {
        write_imagef(out, (int2)(get_group_id(0), get_group_id(1)), partial[0]);
    }
}

__kernel void compute_min_complete(__read_only image2d_t in, __local float4* partial,
                                   __global float4* min_out) {
    int2 const dim = (int2)(get_image_width(in), get_image_height(in));

    int2 const id = (int2)(get_local_id(0), get_local_id(1));
    if (id.x >= dim.x || id.y >= dim.y) return;

    int2 const sz = min(dim, (int2)(get_local_size(0), get_local_size(1)));
    int const offset = id.y*sz.x + id.x;

    partial[offset] = read_imagef(in, gSampler, id);
    barrier(CLK_LOCAL_MEM_FENCE);

    for (int i=(sz.x*sz.y)/2; i>0; i>>=1) {
        if (offset < i) partial[offset] = min(partial[offset], partial[offset+i]);
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    if (id.x == 0 && id.y == 0) *min_out = partial[0];
}

__kernel void compute_max_partial(__read_only image2d_t in, __local float4* partial,
                                  __write_only image2d_t out) {
    int2 const dim = (int2)(get_image_width(in), get_image_height(in));

    int2 const xy = (int2)(get_global_id(0), get_global_id(1));
    if (xy.x >= dim.x || xy.y >= dim.y) return;

    int2 const id = (int2)(get_local_id(0), get_local_id(1));
    if (id.x >= dim.x || id.y >= dim.y) return;

    int2 const sz = min(dim, (int2)(get_local_size(0), get_local_size(1)));
    int const offset = id.y*sz.x + id.x;

    partial[offset] = read_imagef(in, gSampler, xy);
    barrier(CLK_LOCAL_MEM_FENCE);

    for (int i=(sz.x*sz.y)/2; i>0; i>>=1) {
        if (offset < i) partial[offset] = max(partial[offset], partial[offset+i]);
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    if (id.x == 0 && id.y == 0) {
        write_imagef(out, (int2)(get_group_id(0), get_group_id(1)), partial[0]);
    }
}

__kernel void compute_max_complete(__read_only image2d_t in, __local float4* partial,
                                   __global float4* max_out) {
    int2 const dim = (int2)(get_image_width(in), get_image_height(in));

    int2 const id = (int2)(get_local_id(0), get_local_id(1));
    if (id.x >= dim.x || id.y >= dim.y) return;

    int2 const sz = min(dim, (int2)(get_local_size(0), get_local_size(1)));
    int const offset = id.y*sz.x + id.x;

    partial[offset] = read_imagef(in, gSampler, id);
    barrier(CLK_LOCAL_MEM_FENCE);

    for (int i=(sz.x*sz.y)/2; i>0; i>>=1) {
        if (offset < i) partial[offset] = max(partial[offset], partial[offset+i]);
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    if (id.x == 0 && id.y == 0) *max_out = partial[0];
}

