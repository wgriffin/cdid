#include "image.hpp"
#include "image_io.hpp"
#include "image_config.h"

#include <stdexcept>

#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>

namespace image {

    cl::Image2D image::toCL2D(cl::Context& context) {
        if (depth() != 1) throw std::invalid_argument("cannot create Image2D from 3D image");

        cl_channel_order channel_order;
        switch(ncomponents()) {
        case 4: channel_order = CL_RGBA; break;
        case 3: channel_order = CL_RGB; break;
        case 2: channel_order = CL_RG; break;
        case 1: channel_order = CL_R; break;
        default: throw std::invalid_argument("wrong number of channels");
        }

        cl::ImageFormat format(channel_order, CL_FLOAT);
        cl_int flags = CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR;

        float* data = new float[npixels()*ncomponents()];
        for (size_t y=0; y<height(); ++y) {

            float* row = data+y*row_stride();
            for (size_t x=0; x<width(); ++x) {

                float* dp = row+x*ncomponents();
                dp[0] = pixel(0, x, y, 0);
                dp[1] = pixel(1, x, y, 0);
                dp[2] = pixel(2, x, y, 0);
                dp[3] = pixel(3, x, y, 0);
            }
        }

        cl::Image2D image(context, flags, format, width(), height(), row_pitch(), data);
        delete[] data;
        return image;
    }

    void image::allocateFromCL2D(cl::CommandQueue& queue, cl::Image2D const& img) {
        cl_image_format format = img.getImageInfo<CL_IMAGE_FORMAT>();
        if (format.image_channel_data_type != CL_FLOAT) {
            throw std::invalid_argument("only float images are supported");
        }

        int nc;
        switch(format.image_channel_order) {
        case CL_R:
        case CL_Rx:
        case CL_A:
        case CL_INTENSITY:
        case CL_LUMINANCE:
            nc = 1;
            break;

        case CL_RG:
        case CL_RGx:
        case CL_RA:
            nc = 2;
            break;

        case CL_RGB:
        case CL_RGBx:
            nc = 3;
            break;

        case CL_RGBA:
        case CL_ARGB:
        case CL_BGRA:
            nc = 4;
            break;
        }

        size_t w = img.getImageInfo<CL_IMAGE_WIDTH>();
        size_t h = img.getImageInfo<CL_IMAGE_HEIGHT>();
        size_t rp = img.getImageInfo<CL_IMAGE_ROW_PITCH>();

        cl::size_t<3> origin, region;
        origin[0] = origin[1] = origin[2] = 0;
        region[0] = w; region[1] = h; region[2] = 1;

        float* data = new float[w*h*nc];
        queue.enqueueReadImage(img, CL_TRUE, origin, region, rp, 0, data, NULL, NULL);


        allocate(w, h, 1, nc);


        for (size_t y=0; y<height(); ++y) {

            float* row = data+y*row_stride();
            for (size_t x=0; x<width(); ++x) {

                float* dp = row+x*ncomponents();
                switch(format.image_channel_order) {

                case CL_R:
                case CL_Rx:
                case CL_A:
                case CL_INTENSITY:
                case CL_LUMINANCE:
                    pixel(0, x, y, 0) = dp[0];
                    break;

                case CL_RG:
                case CL_RGx:
                case CL_RA:
                    pixel(0, x, y, 0) = dp[0];
                    pixel(1, x, y, 0) = dp[1];
                    break;

                case CL_RGB:
                case CL_RGBx:
                    pixel(0, x, y, 0) = dp[0];
                    pixel(1, x, y, 0) = dp[1];
                    pixel(2, x, y, 0) = dp[2];
                    break;

                case CL_RGBA:
                    pixel(0, x, y, 0) = dp[0];
                    pixel(1, x, y, 0) = dp[1];
                    pixel(2, x, y, 0) = dp[2];
                    pixel(3, x, y, 0) = dp[3];
                    break;

                case CL_ARGB:
                    pixel(0, x, y, 0) = dp[1];
                    pixel(1, x, y, 0) = dp[2];
                    pixel(2, x, y, 0) = dp[3];
                    pixel(3, x, y, 0) = dp[0];
                    break;

                case CL_BGRA:
                    pixel(0, x, y, 0) = dp[2];
                    pixel(1, x, y, 0) = dp[1];
                    pixel(2, x, y, 0) = dp[0];
                    pixel(3, x, y, 0) = dp[3];
                    break;
                }
            }
        }

        delete[] data;
    }

    namespace io {

        void write(cl::CommandQueue& queue, cl::Image2D const& img, std::string const& filename,
              SampleBitDepth bit_depth, FileType file_type) {
            write(image(queue, img), filename, bit_depth, file_type);
        }

    } // namespace io

} // namespace image

