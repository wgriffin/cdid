#include "image_io-internal.hpp"

#include <algorithm>
#include <cctype>
#include <cstdio>
#include <stdexcept>

#include <strings.h>

static image::image read_pgm(std::string const& filename) {
    FILE* fh = fopen(filename.c_str(), "rb");
    if (!fh) throw std::runtime_error("cannot open " + filename);


    char header[3] = { '\0', '\0', '\0' };
    fscanf(fh, "%2s ", header);
    if (strcasecmp(header, "P5") != 0) throw std::runtime_error("error reading pam header (head)");

    int width, height;
    fscanf(fh, "%d %d ", &width, &height);

    int maxval;
    fscanf(fh, "%d ", &maxval);


    unsigned char* data = new unsigned char[width*height];
    fread(data, sizeof(unsigned char), width*height, fh);

    if (ferror(fh)) throw std::runtime_error("cannot read " + filename);
    fclose(fh);


    image::image img(width, height, 1, 1);


    for (size_t y=0; y<img.height(); ++y) {
        unsigned char* row = data + y*width;

        for (size_t x=0; x<img.width(); ++x) {
            unsigned char* pixel = row + x;
            img.pixel(0, x, y, 0) = static_cast<float>(pixel[0])/255.0f;
        }
    }


    delete[] data;
    return img;
}

static image::image read_ppm(std::string const& filename) {
    FILE* fh = fopen(filename.c_str(), "rb");
    if (!fh) throw std::runtime_error("cannot open " + filename);


    char header[3] = { '\0', '\0', '\0' };
    fscanf(fh, "%2s ", header);
    if (strcasecmp(header, "P6") != 0) throw std::runtime_error("error reading pam header (head)");

    int width, height;
    fscanf(fh, "%d %d ", &width, &height);

    int maxval;
    fscanf(fh, "%d ", &maxval);


    size_t const npixels = width*height;
    size_t const pitch = 3*width;

    unsigned char* data = new unsigned char[height*pitch];
    fread(data, sizeof(unsigned char), 3*npixels, fh);

    if (ferror(fh)) throw std::runtime_error("cannot read " + filename);
    fclose(fh);


    image::image img(width, height, 1, 3);


    for (size_t y=0; y<img.height(); ++y) {
        unsigned char* row = data + y*pitch;

        for (size_t x=0; x<img.width(); ++x) {
            unsigned char* pixel = row + x*3;

            switch(img.ncomponents()) {
            case 4: img.pixel(3, x, y, 0) = static_cast<float>(pixel[3])/255.0f;
            case 3: img.pixel(2, x, y, 0) = static_cast<float>(pixel[2])/255.0f;
            case 2: img.pixel(1, x, y, 0) = static_cast<float>(pixel[1])/255.0f;
            case 1: img.pixel(0, x, y, 0) = static_cast<float>(pixel[0])/255.0f;
            }
        }
    }


    delete[] data;
    return img;
}

static image::image read_pam(std::string const& filename) {
    FILE* fh = fopen(filename.c_str(), "rb");
    if (!fh) throw std::runtime_error("cannot open " + filename);


    char header[3] = { '\0', '\0', '\0' };
    fscanf(fh, "%2s ", header);
    if (strcasecmp(header, "P7") != 0) throw std::runtime_error("error reading pam header (head)");


    int width, height, ncomponents, maxval;


    char* token;
    while (!feof(fh)) {
        fscanf(fh, "%as = ", &token);

        if (strcasecmp(token, "WIDTH") == 0) {
            fscanf(fh, "%d ", &width);

        } else if (strcasecmp(token, "HEIGHT") == 0) {
            fscanf(fh, "%d ", &height);

        } else if (strcasecmp(token, "DEPTH") == 0) {
            fscanf(fh, "%d ", &ncomponents);

        } else if (strcasecmp(token, "MAXVAL") == 0) {
            fscanf(fh, "%d ", &maxval);

        } else if (strcasecmp(token, "ENDHDR") == 0) {
            break;

        } else {
            int ch;
            while ((ch = fgetc(fh)) != '\n');
        }
    }


    size_t const npixels = width*height;
    size_t const pitch = ncomponents*width;

    unsigned char* data = new unsigned char[height*pitch];
    fread(data, sizeof(unsigned char), ncomponents*npixels, fh);

    if (ferror(fh)) throw std::runtime_error("cannot read " + filename);
    fclose(fh);


    image::image img(width, height, 1, ncomponents);


    for (size_t y=0; y<img.height(); ++y) {
        unsigned char* row = data + y*pitch;

        for (size_t x=0; x<img.width(); ++x) {
            unsigned char* pixel = row + x*ncomponents;

            switch(img.ncomponents()) {
            case 4: img.pixel(3, x, y, 0) = static_cast<float>(pixel[3])/255.0f;
            case 3: img.pixel(2, x, y, 0) = static_cast<float>(pixel[2])/255.0f;
            case 2: img.pixel(1, x, y, 0) = static_cast<float>(pixel[1])/255.0f;
            case 1: img.pixel(0, x, y, 0) = static_cast<float>(pixel[0])/255.0f;
            }
        }
    }


    delete[] data;
    return img;
}

static image::image read_pfm(std::string const& filename) {
    FILE* fh = fopen(filename.c_str(), "rb");
    if (!fh) throw std::runtime_error("cannot open " + filename);


    char header[3] = { '\0', '\0', '\0' };
    fscanf(fh, "%2s ", header);
    if (strcasecmp(header, "PF") != 0) throw std::runtime_error("error reading pam header (head)");

    int width, height;
    fscanf(fh, "%d %d ", &width, &height);

    float scale;
    fscanf(fh, "%f ", &scale);


    size_t const npixels = width*height;
    size_t const pitch = 3*width;

    float* data = new float[height*pitch];
    fread(data, sizeof(float), 3*npixels, fh);

    if (ferror(fh)) throw std::runtime_error("cannot read " + filename);
    fclose(fh);


    image::image img(width, height, 1, 3);


    for (size_t y=0; y<img.height(); ++y) {
        float* row = data + y*pitch;

        for (size_t x=0; x<img.width(); ++x) {
            float* pixel = row + x*3;

            switch(img.ncomponents()) {
            case 4: img.pixel(3, x, y, 0) = pixel[3];
            case 3: img.pixel(2, x, y, 0) = pixel[2];
            case 2: img.pixel(1, x, y, 0) = pixel[1];
            case 1: img.pixel(0, x, y, 0) = pixel[0];
            }
        }
    }


    delete[] data;
    return img;
}

image::image read_pnm(std::string const& filename) {
    std::string ext = extension(filename);

    if (ext == ".pgm") return read_pgm(filename);
    else if (ext == ".ppm") return read_ppm(filename);
    else if (ext == ".pam") return read_pam(filename);
    else if (ext == ".pfm") return read_pfm(filename);
    else throw std::runtime_error("unsupported extension '" + ext + "'");
}


static void write_pgm(image::image const& img, std::string const& filename) {
    if (img.depth() != 1) throw std::invalid_argument("pgm does not support 3D images");
    if (img.ncomponents() != 1) throw std::invalid_argument("pgm only supports 1 channel images");

    FILE* fh = fopen(filename.c_str(), "wb");
    if (!fh) throw std::runtime_error("cannot open " + filename);


    fprintf(fh, "P5\n");
    fprintf(fh, "%zu %zu\n", img.width(), img.height());
    fprintf(fh, "255\n");


    for (size_t y=0; y<img.height(); ++y) {
        unsigned char row[img.width()];

        for (size_t x=0; x<img.width(); ++x) {
            row[x] = static_cast<unsigned char>(img.pixel(0, x, y, 0)*255.0f);
        }

        fwrite(row, sizeof(row[0]), img.width(), fh);
    }


    if (ferror(fh)) throw std::runtime_error("cannot write " + filename);
    fclose(fh);
}

static void write_ppm(image::image const& img, std::string const& filename) {
    if (img.depth() != 1) throw std::invalid_argument("ppm does not support 3D images");
    if (img.ncomponents() != 3) throw std::invalid_argument("ppm only supports 3 channel images");

    FILE* fh = fopen(filename.c_str(), "wb");
    if (!fh) throw std::runtime_error("cannot open " + filename);


    fprintf(fh, "P6\n");
    fprintf(fh, "%zu %zu\n", img.width(), img.height());
    fprintf(fh, "255\n");


    size_t const row_size = img.width()*img.ncomponents();

    for (size_t y=0; y<img.height(); ++y) {
        unsigned char row[row_size];

        for (size_t x=0; x<img.width(); ++x) {
            unsigned char* pixel = row+x*img.ncomponents();

            switch(img.ncomponents()) {
            case 4: pixel[3] = static_cast<unsigned char>(img.pixel(3, x, y, 0)*255.0f);
            case 3: pixel[2] = static_cast<unsigned char>(img.pixel(2, x, y, 0)*255.0f);
            case 2: pixel[1] = static_cast<unsigned char>(img.pixel(1, x, y, 0)*255.0f);
            case 1: pixel[0] = static_cast<unsigned char>(img.pixel(0, x, y, 0)*255.0f);
            }

        }

        fwrite(row, sizeof(row[0]), row_size, fh);
    }


    if (ferror(fh)) throw std::runtime_error("cannot write " + filename);
    fclose(fh);
}

static void write_pam(image::image const& img, std::string const& filename) {
    if (img.depth() != 1) throw std::invalid_argument("pam does not support 3D images");
    if (img.ncomponents() < 1 || img.ncomponents() > 4) {
        throw std::invalid_argument("pam only supports between 1 and 4 components");
    }

    FILE* fh = fopen(filename.c_str(), "wb");
    if (!fh) throw std::runtime_error("cannot open " + filename);


    fprintf(fh, "P7\n");
    fprintf(fh, "WIDTH %zu\n", img.width());
    fprintf(fh, "HEIGHT %zu\n", img.height());
    fprintf(fh, "DEPTH %zu\n", img.ncomponents());
    fprintf(fh, "MAXVAL 255\n");

    switch(img.ncomponents()) {
    case 4: fprintf(fh, "TUPLTYPE RGB_ALPHA\n"); break;
    case 3: fprintf(fh, "TUPLTYPE RGB\n"); break;
    case 2: fprintf(fh, "TUPLTYPE GRAYSCALE_ALPHA\n"); break;
    case 1: fprintf(fh, "TUPLTYPE GRAYSCALE\n"); break;
    }

    fprintf(fh, "ENDHDR\n");


    size_t const row_size = img.width()*img.ncomponents();

    for (size_t y=0; y<img.height(); ++y) {
        unsigned char row[row_size];

        for (size_t x=0; x<img.width(); ++x) {
            unsigned char* pixel = row+x*img.ncomponents();

            switch(img.ncomponents()) {
            case 4: pixel[3] = static_cast<unsigned char>(img.pixel(3, x, y, 0)*255.0f);
            case 3: pixel[2] = static_cast<unsigned char>(img.pixel(2, x, y, 0)*255.0f);
            case 2: pixel[1] = static_cast<unsigned char>(img.pixel(1, x, y, 0)*255.0f);
            case 1: pixel[0] = static_cast<unsigned char>(img.pixel(0, x, y, 0)*255.0f);
            }

        }

        fwrite(row, sizeof(row[0]), row_size, fh);
    }


    if (ferror(fh)) throw std::runtime_error("cannot write " + filename);
    fclose(fh);
}

static void write_pfm(image::image const& img, std::string const& filename) {
    if (img.depth() != 1) throw std::invalid_argument("pfm does not support 3D images");
    if (img.ncomponents() != 1 && img.ncomponents() != 3) {
        throw std::invalid_argument("pfm only supports 1 and 3 channel images");
    }

    FILE* fh = fopen(filename.c_str(), "wb");
    if (!fh) throw std::runtime_error("cannot open " + filename);


    fprintf(fh, "P%c\n", (img.ncomponents() == 1 ? 'f' : 'F'));
    fprintf(fh, "%zu %zu\n", img.width(), img.height());
#ifdef BIG_ENDIAN
    fprintf(fh, "1.0\n");
#else
    fprintf(fh, "-1.0\n");
#endif


    size_t const row_size = img.width()*img.ncomponents();

    for (size_t y=0; y<img.height(); ++y) {
        float row[row_size];

        for (size_t x=0; x<img.width(); ++x) {
            float* pixel = row+x*img.ncomponents();

            switch(img.ncomponents()) {
            case 4: pixel[3] = img.pixel(3, x, y, 0);
            case 3: pixel[2] = img.pixel(2, x, y, 0);
            case 2: pixel[1] = img.pixel(1, x, y, 0);
            case 1: pixel[0] = img.pixel(0, x, y, 0);
            }

        }

        fwrite(row, sizeof(row[0]), row_size, fh);
    }


    if (ferror(fh)) throw std::runtime_error("cannot write " + filename);
    fclose(fh);
}

void write_pnm(image::image const& img, std::string const& filename,
               image::io::SampleBitDepth bit_depth) {
    std::string ext = extension(filename);

    if (ext == ".pgm") {
        if (bit_depth != image::io::SampleBitDepth_8) {
            throw std::runtime_error("cannot write non-8bit pgm");
        }
        write_pgm(img, filename);

    } else if (ext == ".ppm") {
        if (bit_depth != image::io::SampleBitDepth_8) {
            throw std::runtime_error("cannot write non-8bit ppm");
        }
        write_ppm(img, filename);

    } else if (ext == ".pam") {
        if (bit_depth != image::io::SampleBitDepth_8) {
            throw std::runtime_error("cannot write non-8bit pam");
        }
        write_pam(img, filename);

    } else if (ext == ".pfm") {
        if (bit_depth != image::io::SampleBitDepth_32) {
            throw std::runtime_error("cannot write non-32bit pfm");
        }
        write_pfm(img, filename);

    } else {
        throw std::runtime_error("unsupported extension '" + ext + "'");
    }
}


