#include "image_io-internal.hpp"

#include <cstdio>
#include <stdexcept>

#ifdef _USE_PNG
#include <png.h>

image::image read_png(std::string const& filename) {
    png_structp structp = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!structp) throw std::runtime_error("cannot initialize png library (struct)");

    png_infop infop = png_create_info_struct(structp);
    if (!infop) throw std::runtime_error("cannot initialize png library (info)");


    if (setjmp(png_jmpbuf(structp))) throw std::runtime_error("cannot initialize png io");
    FILE* fh = fopen(filename.c_str(), "rb");
    if (!fh) throw std::runtime_error("cannot open " + filename);


    static size_t const PNG_HEADER_SIZE = 8;
    png_byte header[PNG_HEADER_SIZE];
    fread(header, sizeof(header[0]), PNG_HEADER_SIZE, fh);
    if (png_sig_cmp(header, 0, PNG_HEADER_SIZE) != 0) throw std::runtime_error("invalid header");


    png_init_io(structp, fh);
    png_set_sig_bytes(structp, PNG_HEADER_SIZE);
    png_read_info(structp, infop);


    int width = png_get_image_width(structp, infop);
    int height = png_get_image_height(structp, infop);
    int bit_depth = png_get_bit_depth(structp, infop);
    int color_type = png_get_color_type(structp, infop);

    int ncomponents = 0;
    switch(color_type) {
    case PNG_COLOR_TYPE_GRAY: ncomponents = 1; break;
    case PNG_COLOR_TYPE_GRAY_ALPHA: ncomponents = 2; break;
    case PNG_COLOR_TYPE_RGB: ncomponents = 3; break;
    case PNG_COLOR_TYPE_RGB_ALPHA: ncomponents = 4; break;
    }

    if (ncomponents < 1 || ncomponents > 4) {
        throw std::runtime_error("png only supports between 1 and 4 components");
    }

    if (bit_depth < 8) png_set_packing(structp);
#ifdef BIG_ENDIAN
    // this doesn't make sense to me... I would expect to have to do this on
    // little-endian machines.
    if (bit_depth > 8) png_set_swap(structp);
#endif

    png_read_update_info(structp, infop);
    if (setjmp(png_jmpbuf(structp))) throw std::runtime_error("cannot read bytes");

    png_bytepp rows = (png_bytepp)png_malloc(structp, height*sizeof(png_bytep));
    for (int y=0; y<height; ++y) {
        rows[y] = (png_bytep)png_malloc(structp, png_get_rowbytes(structp, infop));
    }

    png_read_image(structp, rows);

    if (ferror(fh)) throw std::runtime_error("cannot write " + filename);
    fclose(fh);


    image::image img(width, height, 1, ncomponents);

    for (size_t y=0; y<img.height(); ++y) {
        for (size_t x=0; x<img.width(); ++x) {
            png_bytep pixel = (png_bytep)(rows[y] + x*(bit_depth/8)*img.ncomponents());

            switch(bit_depth) {
            case 8:
                switch(img.ncomponents()) {
                case 4: img.pixel(3, x, y, 0) = static_cast<float>(pixel[3])/255.0f;
                case 3: img.pixel(2, x, y, 0) = static_cast<float>(pixel[2])/255.0f;
                case 2: img.pixel(1, x, y, 0) = static_cast<float>(pixel[1])/255.0f;
                case 1: img.pixel(0, x, y, 0) = static_cast<float>(pixel[0])/255.0f;
                }
                break;

            case 16:
                switch(img.ncomponents()) {
                case 4: img.pixel(3, x, y, 0) = static_cast<float>((pixel[6]<<8)|pixel[7])/255.0f;
                case 3: img.pixel(2, x, y, 0) = static_cast<float>((pixel[4]<<8)|pixel[5])/255.0f;
                case 2: img.pixel(1, x, y, 0) = static_cast<float>((pixel[2]<<8)|pixel[3])/255.0f;
                case 1: img.pixel(0, x, y, 0) = static_cast<float>((pixel[0]<<8)|pixel[1])/255.0f;
                }
                break;
            }
        }
    }


    for (size_t y=0; y<img.height(); ++y) png_free(structp, rows[y]);
    png_free(structp, rows);
    png_destroy_read_struct(&structp, &infop, NULL);

    return img;
}

void write_png(image::image const& img, std::string const& filename,
               image::io::SampleBitDepth bit_depth) {
    if (bit_depth == image::io::SampleBitDepth_32) {
        throw std::invalid_argument("png does not support 32bit samples");
    }
    if (img.depth() != 1) throw std::invalid_argument("png does not support 3D images");
    if (img.ncomponents() < 1 || img.ncomponents() > 4) {
        throw std::invalid_argument("png only supports between 1 and 4 components");
    }

    png_structp structp = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!structp) throw std::runtime_error("cannot initialize png library (struct)");

    png_infop infop = png_create_info_struct(structp);
    if (!infop) throw std::runtime_error("cannot initialize png library (info)");


    if (setjmp(png_jmpbuf(structp))) throw std::runtime_error("cannot initialize png io");
    FILE* fh = fopen(filename.c_str(), "wb");
    if (!fh) throw std::runtime_error("cannot open " + filename);
    png_init_io(structp, fh);


    int color_type;
    switch(img.ncomponents()) {
    case 1: color_type = PNG_COLOR_TYPE_GRAY; break;
    case 2: color_type = PNG_COLOR_TYPE_GRAY_ALPHA; break;
    case 3: color_type = PNG_COLOR_TYPE_RGB; break;
    case 4: color_type = PNG_COLOR_TYPE_RGB_ALPHA; break;
    }

    if (setjmp(png_jmpbuf(structp))) throw std::runtime_error("cannot write header");
    png_set_IHDR(structp, infop, img.width(), img.height(), static_cast<int>(bit_depth), color_type,
                 PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

    std::string key = img.comment().first;
    if (!key.empty()) {
        png_text comment;
        comment.compression = PNG_TEXT_COMPRESSION_NONE;
        comment.key = const_cast<char*>(key.c_str());

        std::string text = img.comment().second;
        comment.text_length = text.size();
        if (!text.empty()) comment.text = const_cast<char*>(text.c_str());
        else comment.text = "";

        png_set_text(structp, infop, &comment, 1);
    }

    png_write_info(structp, infop);

#ifdef BIG_ENDIAN
    // this doesn't make sense to me... I would expect to have to do this on
    // little-endian machines.
    if (bit_depth > image::io::SampleBitDepth_8) png_set_swap(structp);
#endif


    png_bytepp rows = (png_bytepp)png_malloc(structp, img.height()*sizeof(png_bytep));

    for (size_t y=0; y<img.height(); ++y) {
        rows[y] = (png_bytep)png_malloc(structp, png_get_rowbytes(structp, infop));

        for (size_t x=0; x<img.width(); ++x) {
            png_bytep pixel = (png_bytep)(rows[y] + x*(static_cast<int>(bit_depth)/8)*img.ncomponents());

            switch(img.ncomponents()) {
            case 4: pixel[3] = static_cast<unsigned char>(img.pixel(3, x, y, 0)*255.0f);
            case 3: pixel[2] = static_cast<unsigned char>(img.pixel(2, x, y, 0)*255.0f);
            case 2: pixel[1] = static_cast<unsigned char>(img.pixel(1, x, y, 0)*255.0f);
            case 1: pixel[0] = static_cast<unsigned char>(img.pixel(0, x, y, 0)*255.0f);
            }
        }
    }

    if (setjmp(png_jmpbuf(structp))) throw std::runtime_error("cannot write bytes");
    png_write_image(structp, rows);


    if (setjmp(png_jmpbuf(structp))) throw std::runtime_error("cannot finish png io");
    png_write_end(structp, NULL);

    for (size_t y=0; y<img.height(); ++y) png_free(structp, rows[y]);
    png_free(structp, rows);
    png_destroy_write_struct(&structp, &infop);

    if (ferror(fh)) throw std::runtime_error("cannot write " + filename);
    fclose(fh);
}

#else // _USE_PNG

image::image read_png(std::string const& filename) {
    throw std::runtime_error("no png support");
}

void write_png(image::image const& img, std::string const& filename,
               image::io::SampleBitDepth bit_depth) {
    throw std::runtime_error("no png support");
}

#endif // _USE_PNG

