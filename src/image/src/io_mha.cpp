#include "image_io-internal.hpp"

#include <cstdio>
#include <cstring>
#include <stdexcept>

#include <strings.h>

image::image read_mha(std::string const& filename) {
    FILE* fh = fopen(filename.c_str(), "rb");
    if (!fh) throw std::runtime_error("cannot open " + filename);


    int const MAX_NDIMS = 3;
    int ndims = -1, ncomponents = -1;
    int dimsize[MAX_NDIMS] = { -1, -1, -1 };

    enum ElemenType {
        MET_UCHAR = 1,
        MET_FLOAT = 4,
        MET_UNKNOWN = -1
    } element_type = MET_UNKNOWN;


    char* token, *value;
    while (!feof(fh)) {
        fscanf(fh, "%as = ", &token);

        if (strcasecmp(token, "NDims") == 0) {
            fscanf(fh, "%d", &ndims);
            if (ndims < 1 || ndims > MAX_NDIMS) {
                throw std::runtime_error("error reading mha header (ndims)");
            }

        } else if (strcasecmp(token, "DimSize") == 0) {
            for (int i=0; i<ndims; ++i) fscanf(fh, "%d", &dimsize[i]);

            for (int i=0; i<ndims; ++i) {
                if (dimsize[i] < 0) throw std::runtime_error("error reading mha header (dimsize)");
            }

            for (int i=ndims; i<MAX_NDIMS; ++i) dimsize[i] = 1;


        } else if (strcasecmp(token, "ElementNumberOfChannels") == 0) {
            fscanf(fh, "%d", &ncomponents);
            if (ncomponents < 1) throw std::runtime_error("error reading mha header (ncomponents)");

        } else if ((strcasecmp(token, "BinaryDataByteOrderMSB") == 0) ||
                   (strcasecmp(token, "ElementByteOrderMSB") == 0)) {
            fscanf(fh, "%as", &value);
#ifdef BIG_ENDIAN
            if (strcasecmp(value, "True") != 0) {
                throw std::runtime_error("mha reading only supports same endian file");
            }
#else
            if (strcasecmp(value, "False") != 0) {
                throw std::runtime_error("mha reading only supports same endian file");
            }
#endif

        } else if (strcasecmp(token, "ElementType") == 0) {
            fscanf(fh, "%as", &value);
            if (strcasecmp(value, "MET_UCHAR") == 0) {
                element_type = MET_UCHAR;
            } else if (strcasecmp(value, "MET_FLOAT") == 0) {
                element_type = MET_FLOAT;
            } else {
                throw std::runtime_error("mha reading only supports UCHAR or FLOAT sample types");
            }

            if (element_type == MET_UNKNOWN) {
                throw std::runtime_error("error reading mha header (element_type)");
            }

        } else if (strcasecmp(token, "ElementDataFile") == 0) {
            fscanf(fh, "%as\n", &value);
            if (strcasecmp(value, "LOCAL") != 0) {
                throw std::runtime_error("mha reading cannot handle external files");
            }
            break;

        } else {
            int ch;
            while ((ch = fgetc(fh)) != '\n');
        }
    }


    size_t const npixels = dimsize[0]*dimsize[1]*dimsize[2];
    size_t const pitch = ncomponents*dimsize[0]*static_cast<int>(element_type);

    void* data = malloc(dimsize[2]*dimsize[1]*pitch);
    if (!data) throw std::bad_alloc();

    fread(data, static_cast<int>(element_type), ncomponents*npixels, fh);
    if (ferror(fh)) throw std::runtime_error("cannot write " + filename);
    fclose(fh);


    image::image img(dimsize[0], dimsize[1], dimsize[2], ncomponents);


    for (size_t z=0; z<img.depth(); ++z) {
        for (size_t y=0; y<img.height(); ++y) {
            void* row = static_cast<unsigned char*>(data) + z*npixels*ncomponents + y*pitch;

            for (size_t x=0; x<img.width(); ++x) {
                switch(element_type) {
                case MET_UCHAR: {
                    unsigned char* pixel = static_cast<unsigned char*>(row)+x*ncomponents;

                    switch(img.ncomponents()) {
                    case 4: img.pixel(3, x, y, z) = static_cast<float>(pixel[3])/255.0f;
                    case 3: img.pixel(2, x, y, z) = static_cast<float>(pixel[2])/255.0f;
                    case 2: img.pixel(1, x, y, z) = static_cast<float>(pixel[1])/255.0f;
                    case 1: img.pixel(0, x, y, z) = static_cast<float>(pixel[0])/255.0f;
                    }

                    break;
                    }

                case MET_FLOAT: {
                    float* pixel = static_cast<float*>(row)+x*ncomponents;

                    switch(img.ncomponents()) {
                    case 4: img.pixel(3, x, y, z) = pixel[3];
                    case 3: img.pixel(2, x, y, z) = pixel[2];
                    case 2: img.pixel(1, x, y, z) = pixel[1];
                    case 1: img.pixel(0, x, y, z) = pixel[0];
                    }

                    break;
                    }

                case MET_UNKNOWN:
                    throw std::logic_error("unreachable");
                }
            }
        }
    }


    free(data);
    return img;
}

void write_mha(image::image const& img, std::string const& filename,
               image::io::SampleBitDepth bit_depth) {
    FILE* fh = fopen(filename.c_str(), "wb");
    if (!fh) throw std::runtime_error("cannot open " + filename);


    fprintf(fh, "ObjectType = Image\n");

    int ndims = 3;
    if (img.depth() == 1) {
        if (img.height() == 1) ndims = 1;
        else ndims = 2;
    }
    fprintf(fh, "NDims = %d\n", ndims);

    fprintf(fh, "DimSize = %zu", img.width());
    if (ndims == 2) fprintf(fh, " %zu", img.height());
    if (ndims == 3) fprintf(fh, " %zu", img.depth());
    fprintf(fh, "\n");

    fprintf(fh, "ElementNumberOfChannels = %zu\n", img.ncomponents());
    fprintf(fh, "BinaryDataByteOrderMSB = ");
#ifdef BIG_ENDIAN
    fprintf(fh, "True");
#else
    fprintf(fh, "False");
#endif
    fprintf(fh, "\n");

    fprintf(fh, "ElementType =");
    switch(bit_depth) {
    case image::io::SampleBitDepth_8: fprintf(fh, "MET_UCHAR"); break;
    case image::io::SampleBitDepth_32: fprintf(fh, "MET_FLOAT"); break;
    }
    fprintf(fh, "\n");

    fprintf(fh, "ElementDataFile = LOCAL\n");


    size_t const byte_depth = static_cast<int>(bit_depth)/8;
    size_t const row_size = img.width()*img.ncomponents();
    void* row = malloc(row_size*byte_depth);
    if (!row) throw std::bad_alloc();


    for (size_t z=0; z<img.depth(); ++z) {
        for (size_t y=0; y<img.height(); ++y) {
            for (size_t x=0; x<img.width(); ++x) {

                switch(bit_depth) {
                case image::io::SampleBitDepth_8: {
                    unsigned char* pixel = static_cast<unsigned char*>(row)+x*img.ncomponents();

                    switch(img.ncomponents()) {
                    case 4: pixel[3] = static_cast<unsigned char>(img.pixel(3, x, y, z)*255.0f);
                    case 3: pixel[2] = static_cast<unsigned char>(img.pixel(2, x, y, z)*255.0f);
                    case 2: pixel[1] = static_cast<unsigned char>(img.pixel(1, x, y, z)*255.0f);
                    case 1: pixel[0] = static_cast<unsigned char>(img.pixel(0, x, y, z)*255.0f);
                    }

                    break;
                    }

                case image::io::SampleBitDepth_32: {
                    float* pixel = static_cast<float*>(row)+x*img.ncomponents();

                    switch(img.ncomponents()) {
                    case 4: pixel[3] = img.pixel(3, x, y, z);
                    case 3: pixel[2] = img.pixel(2, x, y, z);
                    case 2: pixel[1] = img.pixel(1, x, y, z);
                    case 1: pixel[0] = img.pixel(0, x, y, z);
                    }

                    break;
                    }
                }

            }

            fwrite(row, byte_depth, row_size, fh);
        }
    }


    free(row);
    if (ferror(fh)) throw std::runtime_error("cannot write " + filename);
    fclose(fh);
}


