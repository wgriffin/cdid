#include <image.hpp>

#include <algorithm>
#include <cmath>

namespace image {

    float image::max_element(size_t c) {
        return *std::max_element(component(c), component(c)+npixels());
    }

    float image::min_element(size_t c) {
        return *std::min_element(component(c), component(c)+npixels());
    }

    void image::resize_component_count(size_t ncomponents) {
        if (_ncomponents == ncomponents) return;
        _components.resize(_npixels*ncomponents, 0.0f);
        _ncomponents = ncomponents;
    }

    void image::copy_component(size_t src_c, size_t dst_c) {
        std::copy_n(component(src_c), npixels(), component(dst_c));
    }

    void image::clear(float value) {
        std::fill_n(_components.begin(), _components.size(), value);
    }
    
    void image::clear(size_t c, float value) {
        std::fill_n(component(c), npixels(), value);
    }

    void image::rescale(size_t base_component, size_t ncomponents, float min_value, float max_value) {
        float const new_range = max_value-min_value;

        for (size_t c=0; c<ncomponents; ++c) {
            float const old_max = max_element(base_component+c);
            float const old_min = min_element(base_component+c);
            float const old_range = old_max-old_min;

            float* comp = component(base_component+c);
            for (size_t i=0; i<npixels(); ++i) {
                comp[i] = (new_range*(comp[i]-old_min)) / old_range;
            }
        }
    }

    void image::scale_bias(size_t base_component, size_t ncomponents, float scale, float bias) {
        for (size_t c=0; c<ncomponents; ++c) {
            float* comp = component(base_component+c);
            for (size_t i=0; i<npixels(); ++i) comp[i] = scale*comp[i] + bias;
        }
    }

    void image::exponentiate(size_t base_component, size_t ncomponents, float power) {
        for (size_t c=0; c<ncomponents; ++c) {
            float* comp = component(base_component+c);
            for (size_t i=0; i<npixels(); ++i) comp[i] = std::pow(std::max(0.0f, comp[i]), power);
        }
    }


    void image::allocate(size_t width, size_t height, size_t depth, size_t ncomponents) {
        _width = width;
        _height = height;
        _depth = depth;
        _ncomponents = ncomponents;
        _npixels = width*height*depth;
        _components.resize(_npixels*_ncomponents);
    }

    image::image(size_t width, size_t height, size_t depth, size_t ncomponents)
    : _width(width), _height(height), _depth(depth), _ncomponents(ncomponents),
        _npixels(width*height*depth), _components(_npixels*_ncomponents, 0.0f) {
    }

    image::image(size_t width, size_t height, size_t depth, size_t ncomponents, float* data)
    : _width(width), _height(height), _depth(depth), _ncomponents(ncomponents),
        _npixels(width*height*depth), _components(data, data+_npixels*_ncomponents) {
    }

    image::image(cl::CommandQueue& queue, cl::Image2D const& img) {
        allocateFromCL2D(queue, img);
    }

    image::image()
    : _width(0), _height(0), _depth(0), _ncomponents(0), _npixels(0), _components(0) {
    }

    image::image(image const& rhs)
    : _width(rhs._width), _height(rhs._height), _depth(rhs._depth), _ncomponents(rhs._ncomponents),
        _npixels(rhs._npixels), _components(rhs._components) {
    }

    image& image::operator=(image const& rhs) {
        if (this == &rhs) return *this;

        _width = rhs._width;
        _height = rhs._height;
        _depth = rhs._depth;
        _ncomponents = rhs._ncomponents;
        _npixels = rhs._npixels;
        _components = rhs._components;

        return *this;
    }

    image::~image() {
    }

    void swap(image& lhs, image& rhs) {
        using std::swap;
        swap(lhs._width, rhs._width);
        swap(lhs._height, rhs._height);
        swap(lhs._depth, rhs._depth);
        swap(lhs._ncomponents, rhs._ncomponents);
        swap(lhs._npixels, rhs._npixels);
        swap(lhs._components, rhs._components);
    }

} // namespace image

