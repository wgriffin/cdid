#include "image_io-internal.hpp"

#include <cstdio>
#include <new>
#include <stdexcept>

#ifdef _USE_TIFF
#include <tiffio.h>

image::image read_tiff(std::string const& filename) {
    TIFF* th = TIFFOpen(filename.c_str(), "r");
    if (!th) throw std::runtime_error("cannot open " + filename);


    unsigned short bit_depth, sample_format, ncomponents, planar_config, photometric;
    unsigned int depth, height, width;

    if (TIFFGetFieldDefaulted(th, TIFFTAG_BITSPERSAMPLE, &bit_depth) != 1) {
        throw std::runtime_error("error reading TIFF header (bits)");
    }
    if (bit_depth != 32 && bit_depth != 8) {
        throw std::runtime_error("TIFF reading only supports 8 and 32 bits per sample");
    }

    if (TIFFGetFieldDefaulted(th, TIFFTAG_SAMPLEFORMAT, &sample_format) != 1) {
        throw std::runtime_error("error reading TIFF header (format)");
    }
    if (sample_format != SAMPLEFORMAT_UINT && sample_format != SAMPLEFORMAT_IEEEFP) {
        throw std::runtime_error("TIFF reading only supports UINT and IEEEFP sample formats");
    }

    if (bit_depth == 8 && sample_format == SAMPLEFORMAT_IEEEFP) {
        throw std::runtime_error("unsupported format: 8 bits per sample and IEEEFP format");
    }


    if (TIFFGetFieldDefaulted(th, TIFFTAG_IMAGEDEPTH, &depth) != 1) {
        throw std::runtime_error("error reading TIFF header (depth)");
    }
    if (depth < 1 || depth > 3) {
        throw std::runtime_error("TIFF reading only supports depths between 1 and 3");
    }

    if (TIFFGetFieldDefaulted(th, TIFFTAG_IMAGELENGTH, &height) != 1) {
        throw std::runtime_error("error reading TIFF header (height)");
    }
    if (TIFFGetFieldDefaulted(th, TIFFTAG_IMAGEWIDTH, &width) != 1) {
        throw std::runtime_error("error reading TIFF header (width)");
    }
    if (TIFFGetFieldDefaulted(th, TIFFTAG_SAMPLESPERPIXEL, &ncomponents) != 1) {
        throw std::runtime_error("error reading TIFF header (samples)");
    }


    if (ncomponents > 1) {
        if (TIFFGetFieldDefaulted(th, TIFFTAG_PLANARCONFIG, &planar_config) != 1) {
            throw std::runtime_error("error reading TIFF header (planar)");
        }
        if (planar_config != PLANARCONFIG_CONTIG) {
            throw std::runtime_error("TIFF reading only supports contiguous planar configs");
        }
    }


    if (TIFFGetFieldDefaulted(th, TIFFTAG_PHOTOMETRIC, &photometric) != 1) {
        throw std::runtime_error("error reading TIFF header (photometric)");
    }
    if (photometric != PHOTOMETRIC_MINISBLACK && photometric != PHOTOMETRIC_RGB) {
        throw std::runtime_error("TIFF reading only supports MINISBLACK and RGB photometric");
    }

    unsigned short nextra_samples, *extra_samples;
    if ((photometric == PHOTOMETRIC_RGB && ncomponents != 3) ||
        (photometric == PHOTOMETRIC_MINISBLACK && ncomponents != 1)) {
        if (TIFFGetFieldDefaulted(th, TIFFTAG_EXTRASAMPLES, &nextra_samples, &extra_samples) != 1) {
            throw std::runtime_error("error reading TIFF header (extra)");
        }
    }


    image::image img(width, height, depth, ncomponents);


    tdata_t row = (tdata_t)malloc(TIFFScanlineSize(th));
    if (!row) throw std::bad_alloc();

    for (size_t z=0; z<img.depth(); ++z) {

        for (size_t y=0; y<img.height(); ++y) {
            if (TIFFReadScanline(th, row, z*img.depth()+y) != 1) {
                throw std::runtime_error("error reading " + filename);
            }

            for (size_t x=0; x<img.width(); ++x) {

                switch(bit_depth) {
                case 8: { // will be UINT
                    unsigned char* pixel = static_cast<unsigned char*>(row)+x*ncomponents;

                    switch(img.ncomponents()) {
                    case 4: img.pixel(3, x, y, z) = static_cast<float>(pixel[3])/255.0f;
                    case 3: img.pixel(2, x, y, z) = static_cast<float>(pixel[2])/255.0f;
                    case 2: img.pixel(1, x, y, z) = static_cast<float>(pixel[1])/255.0f;
                    case 1: img.pixel(0, x, y, z) = static_cast<float>(pixel[0])/255.0f;
                    }
                    break;
                    }

                case 32: { // can be UINT or IEEEFP
                    switch(sample_format) {
                    case SAMPLEFORMAT_UINT: {
                        unsigned int* pixel = static_cast<unsigned int*>(row)+x*ncomponents;

                        switch(img.ncomponents()) {
                        case 4: img.pixel(3, x, y, z) = static_cast<float>(pixel[3]);
                        case 3: img.pixel(2, x, y, z) = static_cast<float>(pixel[2]);
                        case 2: img.pixel(1, x, y, z) = static_cast<float>(pixel[1]);
                        case 1: img.pixel(0, x, y, z) = static_cast<float>(pixel[0]);
                        }
                        break;
                        }

                    case SAMPLEFORMAT_IEEEFP: {
                        float* pixel = static_cast<float*>(row)+x*ncomponents;

                        switch(img.ncomponents()) {
                        case 4: img.pixel(3, x, y, z) = pixel[3];
                        case 3: img.pixel(2, x, y, z) = pixel[2];
                        case 2: img.pixel(1, x, y, z) = pixel[1];
                        case 1: img.pixel(0, x, y, z) = pixel[0];
                        }
                        break;
                        }
                    }
                    break;
                    }
                }
            }
        }
    }


    free(row);
    TIFFClose(th);
    return img;
}

void write_tiff(image::image const& img, std::string const& filename,
                image::io::SampleBitDepth bit_depth) {
    TIFF* th = TIFFOpen(filename.c_str(), "w");
    if (!th) throw std::runtime_error("cannot open " + filename);


    if (TIFFSetField(th, TIFFTAG_BITSPERSAMPLE, static_cast<int>(bit_depth)) != 1) {
        throw std::runtime_error("error writing TIFF header (bits)");
    }
    if (TIFFSetField(th, TIFFTAG_COMPRESSION, COMPRESSION_NONE) != 1) {
        throw std::runtime_error("error writing TIFF header (comp)");
    }
    if (TIFFSetField(th, TIFFTAG_IMAGEDEPTH, img.depth()) != 1) {
        throw std::runtime_error("error writing TIFF header (depth)");
    }
    if (TIFFSetField(th, TIFFTAG_IMAGELENGTH, img.height()) != 1) {
        throw std::runtime_error("error writing TIFF header (height)");
    }
    if (TIFFSetField(th, TIFFTAG_IMAGEWIDTH, img.width()) != 1) {
        throw std::runtime_error("error writing TIFF header (width)");
    }

    switch(bit_depth) {
    case image::io::SampleBitDepth_8:
        if (TIFFSetField(th, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT) != 1) {
            throw std::runtime_error("error writing TIFF header (format)");
        }
        break;
    case image::io::SampleBitDepth_32:
        if (TIFFSetField(th, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_IEEEFP) != 1) {
            throw std::runtime_error("error writing TIFF header (format)");
        }
        break;
    }

    if (TIFFSetField(th, TIFFTAG_SAMPLESPERPIXEL, img.ncomponents()) != 1) {
        throw std::runtime_error("error writing TIFF header (samples)");
    }
    if (TIFFSetField(th, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG) != 1) {
        throw std::runtime_error("error writing TIFF header (planar)");
    }

    unsigned short extra_samples = EXTRASAMPLE_ASSOCALPHA;
    switch(img.ncomponents()) {
    case 4:
        if (TIFFSetField(th, TIFFTAG_EXTRASAMPLES, 1, &extra_samples) != 1) {
            throw std::runtime_error("error writing TIFF header (extra)");
        }
        /* FALLTHROUGH */

    case 3:
        if (TIFFSetField(th, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB) != 1) {
            throw std::runtime_error("error writing TIFF header (rgb)");
        }
        break;

    case 1:
        if (TIFFSetField(th, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK) != 1) {
            throw std::runtime_error("error writing TIFF header (minisblack)");
        }
        break;
    }


    size_t const row_size = img.width()*img.ncomponents();
    float row[row_size];

    for (size_t z=0; z<img.depth(); ++z) {
        for (size_t y=0; y<img.height(); ++y) {

            for (size_t x=0; x<img.width(); ++x) {
                size_t const offset = x*img.ncomponents();

                row[offset+0] = img.pixel(0, x, y, z);
                row[offset+1] = img.pixel(1, x, y, z);
                row[offset+2] = img.pixel(2, x, y, z);
                row[offset+3] = img.pixel(3, x, y, z);
            }

            switch(bit_depth) {
            case image::io::SampleBitDepth_8: {
                unsigned char ucrow[row_size];
                for (size_t x=0; x<row_size; ++x) ucrow[x] = static_cast<unsigned char>(row[x]*255.0f);
                TIFFWriteScanline(th, ucrow, y);
                break;
                }

            case image::io::SampleBitDepth_32:
                TIFFWriteScanline(th, row, y);
                break;
            }
        }
    }


    TIFFClose(th);
}

#else // _USE_TIFF

image::image read_tiff(std::string const& filename) {
    throw std::runtime_error("no tiff support");
}

void write_tiff(image::image const& img, std::string const& filename,
                image::io::SampleBitDepth bit_depth) {
    throw std::runtime_error("no tiff support");
}

#endif // _USE_TIFF

