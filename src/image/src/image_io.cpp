#include "image_io-internal.hpp"

#include <algorithm>
#include <cctype>
#include <stdexcept>

std::string extension(std::string filename, bool tolower) {
    if (tolower) std::transform(filename.begin(), filename.end(), filename.begin(), ::tolower);
    size_t pos = filename.find_last_of(".");
    if (pos != std::string::npos) filename = filename.substr(pos);
    return filename;
}

namespace image {

    namespace io {

        image read(std::string const& filename) {
            std::string ext = extension(filename);

            if (ext==".pgm"||ext==".ppm"||ext==".pam"||ext==".pfm") return read_pnm(filename);
            else if (ext==".mha") return read_mha(filename);
            else if (ext==".png") return read_png(filename);
            else if (ext==".tiff"||ext==".tif") return read_tiff(filename);
            else throw std::runtime_error("unknown extension: '" + ext + "'");

        }

        void write(image const& img, std::string const& filename, SampleBitDepth bit_depth,
                   FileType file_type) {
            if (file_type == FileType_USE_EXTENSION) {
                std::string ext = extension(filename);

                if (ext==".pgm"||ext==".ppm"||ext==".pam"||ext==".pfm") file_type = FileType_PNM;
                else if (ext==".mha") file_type = FileType_MHA;
                else if (ext==".png") file_type = FileType_PNG;
                else if (ext==".tiff"||ext==".tif") file_type = FileType_TIFF;
                else throw std::runtime_error("unknown extension: '" + ext + "'");
            }


            switch(file_type) {
            case FileType_PNM: return write_pnm(img, filename, bit_depth);
            case FileType_MHA: return write_mha(img, filename, bit_depth);
            case FileType_PNG: return write_png(img, filename, bit_depth);
            case FileType_TIFF: return write_tiff(img, filename, bit_depth);
            case FileType_USE_EXTENSION: throw std::logic_error("unreachable");
            }

        }

    } // namespace io

} // namespace image_io

