#ifndef _IMAGE_IO_INTERNAL_HPP_
#define _IMAGE_IO_INTERNAL_HPP_

#include "image_io.hpp"
#include "image_config.h"

image::image read_pnm(std::string const& filename);
void write_pnm(image::image const& img, std::string const& filename, image::io::SampleBitDepth bit_depth);

image::image read_mha(std::string const& filename);
void write_mha(image::image const& img, std::string const& filename, image::io::SampleBitDepth bit_depth);

image::image read_png(std::string const& filename);
void write_png(image::image const& img, std::string const& filename, image::io::SampleBitDepth bit_depth);

image::image read_tiff(std::string const& filename);
void write_tiff(image::image const& img, std::string const& filename, image::io::SampleBitDepth bit_depth);

std::string extension(std::string filename, bool tolower = true);

#endif // _IMAGE_IO_INTERNAL_HPP_

