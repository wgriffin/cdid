project(image C CXX)
cmake_minimum_required(VERSION 2.8.9)

set(image_VERSION_MAJOR "1")
set(image_VERSION_MINOR "0")
set(image_VERSION_PATCH "0")
set(image_VERSION "${image_VERSION_MAJOR}.${image_VERSION_MINOR}")
set(image_VERSION_FULL "${image_VERSION_MAJOR}.${image_VERSION_MINOR}.${image_VERSION_PATCH}")


option(image_BUILD_TOOLS "Build tools" ON)
option(image_BUILD_DOCS "Build documentation" ON)
option(image_INSTALL "Install the image library" ON)


set(CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake" "${CMAKE_MODULE_PATH}")


list(APPEND image_INCLUDE_DIRS ${OPENCL_INCLUDE_DIRS})
list(APPEND image_LIBRARIES ${OPENCL_LIBRARIES})


find_package(PNG)

set(_USE_PNG 0)
if (PNG_FOUND)
    set(_USE_PNG 1)
    list(APPEND image_INCLUDE_DIRS ${PNG_INCLUDE_DIR})
    list(APPEND image_LIBRARIES ${PNG_LIBRARIES})
endif()


find_package(TIFF)

set(_USE_TIFF 0)
if (TIFF_FOUND)
    set(_USE_TIFF 1)
    list(APPEND image_INCLUDE_DIRS ${TIFF_INCLUDE_DIR})
    list(APPEND image_LIBRARIES ${TIFF_LIBRARIES})
endif()


include(TestBigEndian)
TEST_BIG_ENDIAN(BIG_ENDIAN)

if (${BIG_ENDIAN})
    set(_BIG_ENDIAN 1)
else()
    set(_BIG_ENDIAN 0)
endif()


if (image_BUILD_DOCS)
    find_package(Doxygen)
endif()


set(IMAGE_LIBRARIES ${image_LIBRARIES} CACHE STRING "image dependencies")

configure_file(${image_SOURCE_DIR}/doc/Doxyfile.in
               ${image_BINARY_DIR}/doc/Doxyfile @ONLY)

configure_file(${image_SOURCE_DIR}/src/image_config.h.in
               ${image_BINARY_DIR}/src/image_config.h @ONLY)

configure_file(${image_SOURCE_DIR}/src/imageConfig.cmake.in
               ${image_BINARY_DIR}/src/imageConfig.cmake @ONLY)

configure_file(${image_SOURCE_DIR}/src/imageConfigVersion.cmake.in
               ${image_BINARY_DIR}/src/imageConfigVersion.cmake @ONLY)


add_subdirectory(src)

if (image_BUILD_TOOLS)
    add_subdirectory(tools)
endif()

#if (DOXYGEN_FOUND AND image_BUILD_DOCS)
    #add_subdirectory(doc)
#endif()


if(image_INSTALL)
    install(FILES ${image_SOURCE_DIR}/include/image.hpp
                  ${image_SOURCE_DIR}/include/image_io.hpp
            DESTINATION include)

    install(FILES ${image_BINARY_DIR}/src/imageConfig.cmake
                  ${image_BINARY_DIR}/src/imageConfigVersion.cmake
            DESTINATION lib/cmake/image)

    install(EXPORT imageTargets DESTINATION lib/cmake/image)
endif()

