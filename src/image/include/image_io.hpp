#ifndef _IMAGE_IO_HPP_
#define _IMAGE_IO_HPP_

#include "image.hpp"

#include <string>

namespace image {

    /** \brief io is the image io namespace. */
    namespace io {

        /** \brief the sample bit depth for io. */
        enum SampleBitDepth {
            SampleBitDepth_8 = 8,   /**< Use 8 bits per sample (unsigned char). */
            SampleBitDepth_32 = 32  /**< Use 32 bits per sample (float). */
        };

        /** \brief the file type for io. */
        enum FileType {
            FileType_USE_EXTENSION, /**< Determine the type from the extension. */
            FileType_PNM,           /**< Use PNM. */
            FileType_MHA,           /**< Use MHA. */
            FileType_PNG,           /**< Use PNG. */
            FileType_TIFF           /**< Use TIFF. */
        };

        /** \brief Read an image from \a filename.
         * @param[in] filename the filename to read.
         * @return the image.
         * @throws std::runtime_error
         * @throws std::logic_error
         * @throws std::bad_alloc
         */
        image read(std::string const& filename);

        /** \brief Write \a img to \a filename.
         * @param[in] img the image to write.
         * @param[in] filename the filename to write.
         * @param[in] bit_depth the SampleBitDepth to write.
         * @param[in] file_type the FileType to write.
         * @throws std::invalid_argument
         * @throws std::runtime_error
         * @throws std::logic_error
         * @throws std::bad_alloc
         */
        void write(image const& img, std::string const& filename,
                   SampleBitDepth bit_depth = SampleBitDepth_8,
                   FileType file_type = FileType_USE_EXTENSION);

        /** \brief Write the OpenCL Image2D \a img to \a filename.
         * @param[in] queue the OpenCL command queue to use to read the image.
         * @param[in] img the cl::Image2D image to write.
         * @param[in] filename the filename to write.
         * @param[in] bit_depth the SampleBitDepth to write.
         * @param[in] file_type the FileType to write.
         * @throws cl::Error
         * @throws std::invalid_argument
         * @throws std::runtime_error
         * @throws std::logic_error
         * @throws std::bad_alloc
         */
        void write(cl::CommandQueue& queue, cl::Image2D const& img,
                   std::string const& filename,
                   SampleBitDepth bit_depth = SampleBitDepth_8,
                   FileType file_type = FileType_USE_EXTENSION);

    } // namespace io

} // namespace image

#endif // _IMAGE_IO_HPP_

