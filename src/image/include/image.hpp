#ifndef _IMAGE_HPP_
#define _IMAGE_HPP_

#include <cassert>
#include <cstdlib>
#include <string>
#include <utility>
#include <vector>

// declare here to avoid include OpenCL header.
namespace cl {
    class CommandQueue;
    class Context;
    class Event;
    class Image2D;
}


/** \brief image is the image library namespace. */
namespace image {

    /**
     * \brief 1d, 2d, or 3d floating-point image container.
     */
    class image {
    public:
        /** \brief Get the pixel width of this image.
         * @return the pixel width of this image. */
        size_t width() const { return _width; }
        /** \brief Get the pixel height of this image.
         * @return the pixel height of this image. */
        size_t height() const { return _height; }
        /** \brief Get the pixel depth of this image.
         * @return the pixel depth of this image. */
        size_t depth() const { return _depth; }
        /** \brief Get the number of components of this image.
         * @return the number of components of this image. */
        size_t ncomponents() const { return _ncomponents; }
        /** \brief Get the total number of pixels of this image.
         * @return the total number of pixels of this image. */
        size_t npixels() const { return _npixels; }
        /** \brief Get the row pixel stride of this image.
         * @return the row pixel stride of this image. */
        size_t row_stride() const { return width()*ncomponents(); }
        /** \brief Get the row pitch in bytes of this image.
         * @return the row pitch in bytes of this image. */
        size_t row_pitch() const { return width()*ncomponents()*sizeof(float); }
        /** \brief Get the slice pitch in bytes of this image.
         * @return the slice pitch in bytes of this image. */
        size_t slice_pitch() const { return height()*row_pitch(); }


        /** \brief Get a pointer to component \a c.
         * @return a pointer to component \a c. */
        float* component(size_t c) { return _components.data() + _npixels*c; }
        /** \brief Get a pointer to component \a c.
         * @return a pointer to component \a c. */
        float const* component(size_t c) const { return _components.data() + _npixels*c; }

        /** \brief Get a pointer to plane \a z of component \a c.
         * @return a pointer to plane \a z of component \a c. */
        float* plane(size_t c, size_t z) { return component(c) + z*_width*_height; }
        /** \brief Get a pointer to plane \a z of component \a c.
         * @return a pointer to plane \a z of component \a c. */
        float const* plane(size_t c, size_t z) const { return component(c) + z*_width*_height; }

        /** \brief Get a pointer to scanline \a y of plane \a z of component \a c.
         * @return a pointer to scanline \a y of plane \a z of component \a c. */
        float* scanline(size_t c, size_t y, size_t z) { return plane(c, z) + y*_width; }
        /** \brief Get a pointer to scanline \a y of plane \a z of component \a c.
         * @return a pointer to scanline \a y of plane \a z of component \a c. */
        float const* scanline(size_t c, size_t y, size_t z) const { return plane(c, z) + y*_width; }

        /** \brief Get the pixel of component \a c at (\a x, \a y, \a z).
         * @return the pixel of component \a c at (\a x, \a y, \a z). */
        float pixel(size_t c, size_t x, size_t y, size_t z) const { return pixel(c, index(x, y, z)); }
        /** \brief Get the pixel of component \a c at (\a x, \a y, \a z).
         * @return the pixel of component \a c at (\a x, \a y, \a z). */
        float& pixel(size_t c, size_t x, size_t y, size_t z) { return pixel(c, index(x, y, z)); }
        /** \brief Get the pixel of component \a c at \a idx.
         * @return the pixel of component \a c at \a idx. */
        float pixel(size_t c, size_t idx) const { return pixel(c*_npixels + idx); }
        /** \brief Get the pixel of component \a c at \a idx.
         * @return the pixel of component \a c at \a idx. */
        float& pixel(size_t c, size_t idx) { return pixel(c*_npixels + idx); }
        /** \brief Get the pixel at \a idx.
         * @return the pixel at \a idx. */
        float pixel(size_t idx) const {
            assert(idx < _components.size());
            return _components[idx];
        }
        /** \brief Get the pixel at \a idx.
         * @return the pixel at \a idx. */
        float& pixel(size_t idx) {
            assert(idx < _components.size());
            return _components[idx];
        }

        /** \brief Compute the index of pixel (\a x, \a y, \a z).
         * @return the index of pixel (\a x, \a y, \a z). */
        size_t index(size_t x, size_t y, size_t z) const {
            assert(x<_width);
            assert(y<_height);
            assert(z<_depth);
            return (z*_height + y)*_width + x;
        }


        using comment_t = std::pair<std::string, std::string>;

        /** \brief Get the comment for this image.
         * @return the comment of this image. */
        comment_t comment() const { return _comment; }

        /** \brief Set the comment for this image.
         * @param[in] comment the comment for this image. */
        void comment(comment_t comment) { _comment = comment; }


        /** \brief Get the max element of a component.
         * @param[in] c the component to get the max of.
         * @return the max element of component \a c. */
        float max_element(size_t c);

        /** \brief Get the min element of a component.
         * @param[in] c the component to get the min of.
         * @return the min element of component \a c. */
        float min_element(size_t c);


        /** \brief Resize the the number of components of this image.
         * @param[in] ncomponents the new number of components.
         */
        void resize_component_count(size_t ncomponents);

        /** \brief Copy one component to another.
         * @param[in] src_c the src component.
         * @param[in] dst_c the dst component.
         */
        void copy_component(size_t src_c, size_t dst_c);

        /** \brief Clear this entire image to \a value.
         * @param[in] value the value to clear the image to. */
        void clear(float value);

        /** \brief Clear component \a component to \a value in this image.
         * @param[in] c the component to clear.
         * @param[in] value the value to clear the image to. */
        void clear(size_t c, float value);

        /** \brief Rescale the pixels in a number of components to a range.
         * @param[in] base_component the first component to scale/bias.
         * @param[in] ncomponents the number of components to scale/bias.
         * @param[in] min_value the new minimum pixel value.
         * @param[in] max_value the new maximum pixel value. */
        void rescale(size_t base_component, size_t ncomponents, float min_value, float max_value);

        /** \brief Scale and bias the pixels in a number of components of this image.
         * @param[in] base_component the first component to scale/bias.
         * @param[in] ncomponents the number of components to scale/bias.
         * @param[in] scale the scale value.
         * @param[in] bias the bias value.
         */
        void scale_bias(size_t base_component, size_t ncomponents, float scale, float bias);

        /** \brief Exponentiate the pixels in a number of components of this image.
         * @param[in] base_component the first component to scale/bias.
         * @param[in] ncomponents the number of components to scale/bias.
         * @param[in] power the power value.
         */
        void exponentiate(size_t base_component, size_t ncomponents, float power);

        /** \brief Convert the pixels in a number of components to linear values.
         * @param[in] base_component the first component to scale/bias.
         * @param[in] ncomponents the number of components to scale/bias.
         * @param[in] gamma the gamma value. */
        void to_linear(size_t base_component, size_t ncomponents, float gamma = 2.2f) {
            exponentiate(base_component, ncomponents, gamma);
        }

        /** \brief Convert the pixels in a number of components to gamma values.
         * @param[in] base_component the first component to scale/bias.
         * @param[in] ncomponents the number of components to scale/bias.
         * @param[in] gamma the gamma value. */
        void to_gamma(size_t base_component, size_t ncomponents, float gamma = 2.2f) {
            exponentiate(base_component, ncomponents, 1/gamma);
        }


        /** \brief Convert this image into an OpenCL 2D image.
         * @param[in] context the OpenCL context to use.
         * @return the cl::Image2D.
         * @throws cl::Error
         * @throws std::runtime_error
         * @throws std::invalid_argument
         */
        cl::Image2D toCL2D(cl::Context& context);

        /** \brief Allocate this image from an OpenCL 2D image.
         * @param[in] queue the OpenCL command queue to use to read the image.
         * @param[in] img the cl::Image2D image to read.
         * @throws cl::Error
         * @throws std::runtime_error
         * @throws std::invalid_argument
         */
        void allocateFromCL2D(cl::CommandQueue& queue, cl::Image2D const& img);


        /** \brief Allocate memory for an image.
         * @param[in] width the width of the new image.
         * @param[in] height the height of the new image.
         * @param[in] depth the depth of the image.
         * @param[in] ncomponents the number of components of the image.
         */
        void allocate(size_t width, size_t height, size_t depth, size_t ncomponents);

        /** \brief Create a new empty image (allocate the memory).
         * @param[in] width the width of the new image.
         * @param[in] height the height of the new image.
         * @param[in] depth the depth of the image.
         * @param[in] ncomponents the number of components of the image.
         */
        image(size_t width, size_t height, size_t depth, size_t ncomponents);

        /** \brief Create a new image from \a data.
         * @param[in] width the width of the new image.
         * @param[in] height the height of the new image.
         * @param[in] depth the depth of the image.
         * @param[in] ncomponents the number of components of the image.
         * @param[in] data the data of the image.
         */
        image(size_t width, size_t height, size_t depth, size_t ncomponents, float* data);

        /** \brief Create a new image from \a img.
         * @param[in] queue the OpenCL command queue to use to read the image.
         * @param[in] img the cl::Image2D image to read.
         * @throws cl::Error
         * @throws std::runtime_error
         * @throws std::invalid_argument
         */
        image(cl::CommandQueue& queue, cl::Image2D const& img);

        /** \brief Create an empty image (do not allocate memory). */
        image();

        /** \brief Copy constructor. */
        image(image const&);
        /** \brief Copy assignment operator. */
        image& operator=(image const&);
        /** Destructor. */
        ~image();

        /** \brief Swap two images. */
        friend void swap(image&, image&);

    private:
        size_t _width, _height, _depth, _ncomponents, _npixels;
        std::vector<float> _components;
        comment_t _comment;
    };

} // namespace image

#endif // _IMAGE_HPP_

