#include <image_io.hpp>

#include <iostream>

#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <string.h>
#include <strings.h>

static bool safe_stoui(char const* str, unsigned int* result) {
    char* end;
    errno = 0;
    long int value = strtol(str, &end, 0);

    if (value == LONG_MAX && errno == ERANGE) {
        return false;
    } else if (value > INT_MAX) {
        errno = ERANGE;
        return false;
    } else if (errno == EINVAL) {
        return false;
    } else if (!(str != '\0' && *end == '\0')) {
        errno = EINVAL;
        return false;
    }

    *result = (int)(value);
    return true;
}


int main(int argc, char* argv[]) {
    unsigned int raw_bit_depth = 0;
    image::io::SampleBitDepth bit_depth = image::io::SampleBitDepth_8;
    image::io::FileType format = image::io::FileType_USE_EXTENSION;


    static char const* const sopts = "hb:f:";
    static struct option lopts[] = {
        { "help", no_argument, NULL, 'h' },
        { "bitdepth", required_argument, NULL, 'b' },
        { "format", required_argument, NULL, 'f' },
        { NULL, 0, NULL, 0 }
    };

    int opt, optidx;
    while ((opt = getopt_long(argc, argv, sopts, lopts, &optidx)) != -1) {
        switch(opt) {

        case 'b':
            if (!safe_stoui(optarg, &raw_bit_depth)) {
                std::cerr << "invalid bit depth '" << optarg << "': " << strerror(errno) << std::endl;
                exit(EXIT_FAILURE);
            }

            if (raw_bit_depth == 8) bit_depth = image::io::SampleBitDepth_8;
            else if (raw_bit_depth == 32) bit_depth = image::io::SampleBitDepth_32;
            else {
                std::cerr << "invalid bit depth '" << optarg << "'" << std::endl;
                exit(EXIT_FAILURE);
            }
            break;

        case 'f':
            if (strcasecmp(optarg, "PNM") == 0) format = image::io::FileType_PNM;
            else if (strcasecmp(optarg, "PGM") == 0) format = image::io::FileType_PNM;
            else if (strcasecmp(optarg, "PPM") == 0) format = image::io::FileType_PNM;
            else if (strcasecmp(optarg, "PAM") == 0) format = image::io::FileType_PNM;
            else if (strcasecmp(optarg, "PFM") == 0) format = image::io::FileType_PNM;
            else if (strcasecmp(optarg, "MHA") == 0) format = image::io::FileType_MHA;
            else if (strcasecmp(optarg, "PNG") == 0) format = image::io::FileType_PNG;
            else if (strcasecmp(optarg, "TIF") == 0) format = image::io::FileType_TIFF;
            else if (strcasecmp(optarg, "TIFF") == 0) format = image::io::FileType_TIFF;
            else {
                std::cerr << "invalid format '" << optarg << "'" << std::endl;
                exit(EXIT_FAILURE);
            }
            break;

        case 'h':
        case '?':
        default:
            std::cerr << "usage: " << argv[0] << " [options] input output" << std::endl;
            std::cerr << "options: " << std::endl;
            std::cerr
                << "    -b,--bitdepth B    use B as the output bit depth (default is 8bit)"
                << std::endl;
            std::cerr
                << "    -f,--format FMT    use FMT for the output format (default is use extension)"
                << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    if (optind+1 >= argc) {
        std::cerr << "input and output required" << std::endl;
        exit(EXIT_FAILURE);
    }


    char const* const srcFN = argv[optind++];
    char const* const dstFN = argv[optind];


    int ec = EXIT_SUCCESS;
    try {
        image::image src = image::io::read(srcFN);
        image::io::write(src, dstFN, bit_depth, format);
    } catch (std::exception const& e) {
        std::cerr << "error converting '" << srcFN << "' to '" << dstFN << "': "
            << e.what() << std::endl;
        ec = EXIT_FAILURE;
    }

    return ec;
}

