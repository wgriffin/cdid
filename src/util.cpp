#include "util.hpp"
#include "resampler.h"

#include <algorithm>
#include <cmath>
#include <climits>
#include <cstdlib>
#include <iomanip>
#include <memory>

#include <errno.h>
#include <unistd.h>

#include <glob.h>
#include <sys/types.h>
#include <sys/stat.h>

static std::string const SEPARATOR("/");

bool read_glob(char const* glob_pattern, std::vector<std::string>& entries) {
    glob_t globs;
    int ec = glob(glob_pattern, GLOB_TILDE, NULL, &globs);

    switch(ec) {
    case GLOB_NOSPACE: throw std::bad_alloc(); break;
    case GLOB_ABORTED: return false;
    case GLOB_NOMATCH: return true;
    }

    struct stat st;
    for (size_t i=0; i<globs.gl_pathc; ++i) {
        stat(globs.gl_pathv[i], &st);
        if (S_ISREG(st.st_mode)) entries.push_back(globs.gl_pathv[i]);
    }

    globfree(&globs);
    return true;
}

#include <dirent.h>
#include <malloc.h>
#include <string.h>

bool read_dir(char const* dirname, std::vector<std::string>& entries) {
    DIR* dir = opendir(dirname);
    if (!dir) return false;

    size_t entry_size = offsetof(struct dirent, d_name) + pathconf(dirname, _PC_NAME_MAX) + 1;
    dirent* entry = reinterpret_cast<dirent*>(malloc(entry_size));
    if (!entry) throw std::bad_alloc();

    dirent* result;
    int err;
    while ((err = readdir_r(dir, entry, &result)) == 0) {
        if (result == NULL) break;
        if (strncmp(entry->d_name, ".", 1) == 0) continue;
        if (strncmp(entry->d_name, "..", 2) == 0) continue;
        entries.push_back(dirname+SEPARATOR+entry->d_name);
    }

    free(entry);
    return (err == 0);
}

std::string dirname(std::string pathname) {
    pathname.erase(pathname.find_last_of(SEPARATOR)+1);
    return pathname;
}

std::string basename(std::string pathname, bool strip_extension) {
    pathname.erase(0, pathname.find_last_of(SEPARATOR)+1);
    if (strip_extension) pathname.erase(pathname.find_last_of("."));
    return pathname;
}

bool safe_stoui(char const* str, unsigned int* result) {
    char* end;
    errno = 0;
    long int value = strtol(str, &end, 0);

    if (value == LONG_MAX && errno == ERANGE) {
        return false;
    } else if (value > INT_MAX) {
        errno = ERANGE;
        return false;
    } else if (errno == EINVAL) {
        return false;
    } else if (!(str != '\0' && *end == '\0')) {
        errno = EINVAL;
        return false;
    }

    *result = (int)(value);
    return true;
}

bool safe_stof(char const* str, float* result) {
    char* end;
    errno = 0;
    float value = strtof(str, &end);

    if ((value == HUGE_VALF || value == 0) && errno == ERANGE) {
        return false;
    } else if (!(str != '\0' && *end == '\0')) {
        errno = EINVAL;
        return false;
    }

    *result = value;
    return true;
}

struct monotonic_increase {
public:
    monotonic_increase(int start = 0) : _value(start) {}

    int operator() () { return _value++; }

private:
    int _value;
};

struct scale_bias {
public:
    scale_bias(float scale, float bias) : _scale(scale), _bias(bias) {}

    float operator() (float value) { return value*_scale + _bias; }

private:
    float _scale, _bias;
};

std::vector<float> linspace(float start, float stop, size_t num, bool endpoint) {
    std::vector<float> result(num);
    if (num <= 0) return result;

    if (endpoint) {
        if (num == 1) {
            result[0] = start;
            return result;
        }

        float step = (stop-start)/static_cast<float>(num-1);
        std::generate_n(result.begin(), num, monotonic_increase());
        std::transform(result.begin(), result.end(), result.begin(), scale_bias(step, start));
    } else {
        float step = (stop-start)/static_cast<float>(num);
        std::generate_n(result.begin(), num, monotonic_increase());
        std::transform(result.begin(), result.end(), result.begin(), scale_bias(step, start));
    }

    return result;
}

correlation_kernel gaussian_kernel_1d(float sigma, float truncate) {
    float sd = sigma;
    int const lw = static_cast<int>(truncate * sd + 0.5);

    std::vector<float> weights(2*lw+1);
    weights[lw] = 1;

    float sum = 1.0;
    sd *= sd;

    for (int ii=1; ii<lw+1; ++ii) {
        float const tmp = std::exp(-0.5f * static_cast<float>(ii*ii) / sd);
        weights[lw+ii] = tmp;
        weights[lw-ii] = tmp;
        sum += 2.0*tmp;
    }

    for (int ii=0; ii<2*lw+1; ++ii) weights[ii] /= sum;

    return std::make_pair(weights.size(), weights);
}

correlation_kernel box_kernel_1d(int width) {
    std::vector<float> weights(width);
    std::fill_n(weights.begin(), width, 1/static_cast<float>(width));
    return std::make_pair(weights.size(), weights);
}

std::ostream& operator<<(std::ostream& os, std::vector<float> const& v) {
    for (size_t i=0; i<v.size(); ++i) {
        os << std::scientific << std::showpos << std::setprecision(8) << v[i];
        if (i<v.size()-1) os << ", ";
    }
    return os;
}

image::image resize(image::image const& src, size_t dst_w, size_t dst_h,
    float clamp_min, float clamp_max, char const* kernel, Resampler::Boundary_Op boundary_op) {
    image::image dst(dst_w, dst_h, src.depth(), src.ncomponents());

    std::vector<std::unique_ptr<Resampler>> resamplers(src.ncomponents());
    std::vector<std::vector<float>> src_samples(src.ncomponents());

    resamplers[0] = std::unique_ptr<Resampler>(
        new Resampler(src.width(), src.height(), dst_w, dst_h, boundary_op, clamp_min, clamp_max,
                      kernel));
    src_samples[0].resize(src.width());

    for (size_t c=1; c<src.ncomponents(); ++c) {
        resamplers[c] = std::unique_ptr<Resampler>(
            new Resampler(src.width(), src.height(), dst_w, dst_h, boundary_op, clamp_min, clamp_max,
                          kernel, resamplers[0]->get_clist_x(), resamplers[0]->get_clist_y()));
        src_samples[c].resize(src.width());
    }

    for (size_t src_y=0, dst_y=0; src_y<src.height(); ++src_y) {

        for (size_t c=0; c<src.ncomponents(); ++c) {
            for (size_t src_x=0; src_x<src.width(); ++src_x) {
                src_samples[c][src_x] = src.pixel(c, src_x, src_y, 0);
            }

            if (!resamplers[c]->put_line(src_samples[c].data())) {
                throw std::runtime_error{ "unable to scale line " + std::to_string(src_y) };
            }
        }

        for (;;) {
            size_t ci=0;
            for (; ci<src.ncomponents(); ++ci) {
                auto dst_samples = resamplers[ci]->get_line();
                if (!dst_samples) break;

                for (size_t dst_x=0; dst_x<dst_w; ++dst_x) {
                    dst.pixel(ci, dst_x, dst_y, 0) = dst_samples[dst_x];
                }
            }
            if (ci<src.ncomponents()) break;

            dst_y++;
        }

    }

    return dst;
}

