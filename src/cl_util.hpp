#ifndef _CL_UTIL_HPP_
#define _CL_UTIL_HPP_

#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>

#include <iosfwd>
#include <utility>

cl::Context init_cl(cl_int deviceType = CL_DEVICE_TYPE_GPU);

cl::Program::Sources read_source(std::string const& filename);
cl::Program build_program(cl::Context& context, std::string const& filename,
                          std::string const& flags = "");

std::string error_string(cl_int error);


std::pair<cl::NDRange, cl::NDRange> compute_global_local(size_t width, size_t height, cl_int2 nthreads);


std::pair<cl::Buffer, cl::Event>
compute_reduction(cl::CommandQueue& queue, cl::Image2D& image, size_t local_size,
                  cl::Kernel partial_kernel, cl::Kernel complete_kernel,
                  cl_int2 nthreads, VECTOR_CLASS<cl::Event>* wait_events = NULL);

std::pair<cl::Image2D, cl::Event>
correlate2d(cl::CommandQueue& queue, cl::Image2D& image, cl::Sampler sampler,
            cl::Buffer& filter, cl_int2 filter_width, cl_uint4 filter_mask,
            cl::Kernel kernel_xy, cl_int2 nthreads, VECTOR_CLASS<cl::Event>* wait_events = NULL);

std::pair<cl::Image2D, cl::Event>
correlate2d(cl::CommandQueue& queue, cl::Image2D& image, cl::Sampler sampler,
            cl::Buffer& filter, int filter_width, cl_uint4 filter_mask,
            cl::Kernel kernel_x, cl::Kernel kernel_y,
            cl_int2 nthreads, VECTOR_CLASS<cl::Event>* wait_events = NULL);

inline cl::Event enqueue(cl::CommandQueue queue, cl::Kernel kernel,
    cl::NDRange global_work_offset, cl::NDRange global_work_size, cl::NDRange local_work_size,
    VECTOR_CLASS<cl::Event>* wait_events, size_t /*arg_idx*/) {
    cl::Event event;
    queue.enqueueNDRangeKernel(kernel, global_work_offset, global_work_size, local_work_size,
                               wait_events, &event);
    return event;
}

template <typename T, typename... Args>
inline cl::Event enqueue(cl::CommandQueue queue, cl::Kernel kernel,
    cl::NDRange global_work_offset, cl::NDRange global_work_size, cl::NDRange local_work_size,
    VECTOR_CLASS<cl::Event>* wait_events, size_t arg_idx, T arg, Args... args) {
    kernel.setArg(arg_idx, arg);
    return enqueue(queue, kernel, global_work_offset, global_work_size, local_work_size,
                   wait_events, arg_idx+1, args...);
}

std::ostream& operator<<(std::ostream&, cl_float4 const&);

std::ostream& operator<<(std::ostream&, cl::Event const&);

#endif // _CLUTIL_HPP_

