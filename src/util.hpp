#ifndef _UTIL_HPP_
#define _UTIL_HPP_

#include "resampler.h"

#include <image.hpp>

#include <cmath>
#include <ostream>
#include <string>
#include <vector>

#include <stdlib.h>
#include <stdint.h>

bool read_glob(char const* glob_pattern, std::vector<std::string>& entries);
bool read_dir(char const* dirname, std::vector<std::string>& entries);

std::string dirname(std::string pathname);
std::string basename(std::string pathname, bool strip_extension = true);

bool safe_stoui(char const* str, unsigned int* result);
bool safe_stof(char const* str, float* result);

std::vector<float> linspace(float start, float stop, size_t num, bool endpoint=true);

typedef std::pair<int, std::vector<float>> correlation_kernel;

correlation_kernel gaussian_kernel_1d(float sigma, float truncate = 4);
correlation_kernel box_kernel_1d(int width);

std::ostream& operator<<(std::ostream&, std::vector<float> const&);

image::image resize(image::image const& src, size_t dst_w, size_t dst_h,
    float clamp_min = 0.0f, float clamp_max = 1.0f, char const* kernel = "kaiser",
    Resampler::Boundary_Op boundary_op = Resampler::BOUNDARY_CLAMP);

template <typename T>
inline T deg2rad(T d) { return d*M_PI/180.0; }

template <typename T>
inline T rad2deg(T r) { return r*180.0/M_PI; }

#endif // _UTIL_HPP_

