#ifndef _DISTORTIONS_HPP_
#define _DISTORTIONS_HPP_

#include "cl_util.hpp"
#include "util.hpp"

#include <image_io.hpp>

#include <random>
#include <sstream>

struct distortion {
    static void undistort(distortion& task);

    static void cd(int shift, distortion& task);
    static void dsab(int blocksize, distortion& task);
    static void hr(float kappa, distortion& task);
    static void gbab(float sigma, distortion& task);
    static void qab(int nbins, distortion& task);

    template <typename DG, typename PG, typename F>
    static std::vector<distortion> distort(cl::CommandQueue queue, cl_int2 nthreads,
        std::string const& kernel_dir, std::vector<std::string> const& refimgnames,
        std::string const& outdir, unsigned int num_distortions, DG is_distorted, PG param_gen, F work);

    template <typename P>
    static void write(std::vector<distortion> const& distortions, std::string const& filename, P p);

    distortion() = default;

    static std::mt19937 gen;
    static cl_int2 nthreads;
    static cl::CommandQueue queue;
    static cl::Program color_space, image_stats, correlation, distortions;

    std::string srcFN, finFN;
    cl::size_t<3> origin, region;

    cl::NDRange global, local;

    image::image srcIMGhost;

    cl::Image2D srcIMG, convIMG, dstIMG, finIMG;
    VECTOR_CLASS<cl::Event> convert_event;

    std::pair<cl::Buffer, cl::Buffer> min_max;
    VECTOR_CLASS<cl::Event> min_max_events;

    cl_float4 min, max;
    cl_uint nbins;

    float sigma;

    float noise;

    int shift;

    float kappa;

    int blocksize;

    VECTOR_CLASS<cl::Event> distort_event;

    bool is_undistorted;


    distortion(std::string const& srcFN_, std::string const& finFN_);

    void read_input();

    void enqueue_convert(char const* kernel_name, cl::Image2D& in, cl::Image2D& out,
                         VECTOR_CLASS<cl::Event> *wait_events = NULL);

    void enqueue_min_max(VECTOR_CLASS<cl::Event> *wait_events = NULL);

    template <typename T>
    void complete_task(VECTOR_CLASS<cl::Event> *wait_events, std::string name, T t);

};


#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <sstream>

template <typename DG, typename PG, typename F>
std::vector<distortion> distortion::distort(cl::CommandQueue queue, cl_int2 nthreads,
    std::string const& kernel_dir, std::vector<std::string> const& refimgnames,
    std::string const& outdir, unsigned int num_distortions, DG is_distorted, PG param_gen, F work) {
    std::vector<distortion> tasks(num_distortions);

    auto context = queue.getInfo<CL_QUEUE_CONTEXT>();
    auto flags = "-I" + kernel_dir;

    auto color_space = build_program(context, kernel_dir + "color_space.cl", flags);
    auto image_stats = build_program(context, kernel_dir + "image_stats.cl", flags);
    auto correlation = build_program(context, kernel_dir + "correlation.cl", flags);
    auto distortions = build_program(context, kernel_dir + "distortions.cl", flags);

    for (size_t i=0; i<tasks.size(); ++i) {
        try {

            std::ostringstream oss;
            oss << outdir << "img" << std::setw(3) << std::setfill('0') << i << ".png";

            tasks[i].nthreads = nthreads;
            tasks[i].queue = queue;
            tasks[i].color_space = color_space;
            tasks[i].image_stats = image_stats;
            tasks[i].correlation = correlation;
            tasks[i].distortions = distortions;

            tasks[i] = distortion{ refimgnames[i%refimgnames.size()], oss.str() };

        } catch (cl::Error const& err) {
            std::cerr << "error on '" << tasks[i].finFN << "' (" << i << ") ("
                << err.what() << "): " << error_string(err.err()) << " (" << err.err() << ")\n";
            continue;
        } catch (std::exception const& err) {
            std::cerr << "error on '" << tasks[i].finFN << "' (" << i << ") : "
                << err.what() << "\n";
            continue;
        }
    }

    #pragma omp parallel for schedule(dynamic)
    for (size_t i=0; i<tasks.size(); ++i) {
        try {

            if (is_distorted(i)) work(param_gen(), tasks[i]);
            else distortion::undistort(tasks[i]);

        } catch (cl::Error const& err) {
            std::cerr << "error on '" << tasks[i].finFN << "' (" << i << ") ("
                << err.what() << "): " << error_string(err.err()) << " (" << err.err() << ")\n";
            continue;
        } catch (std::exception const& err) {
            std::cerr << "error on '" << tasks[i].finFN << "' (" << i << "): "
                << err.what() << "\n";
            continue;
        }
    }

    return tasks;
}

template <typename P>
void distortion::write(std::vector<distortion> const& distortions, std::string const& filename, P p) {
    std::ofstream ofs{ filename.c_str(), std::ios::binary };
    if (!ofs) throw std::runtime_error{ "cannot open " + filename };

    for (auto& distortion : distortions) {
        ofs << basename(distortion.srcFN, false) << " " << basename(distortion.finFN, false) << " ";
        if (distortion.is_undistorted) ofs << " *";
        else ofs << std::bind(p, distortion)();
        ofs << "\n";
    }
}

template <typename T>
void distortion::complete_task(VECTOR_CLASS<cl::Event> *wait_events, std::string name, T t) {
    cl::Event::waitForEvents(*wait_events);
    image::image img(queue, finIMG);

    img.resize_component_count(3);
    img.to_gamma(0, 3);

    std::ostringstream comment;
    comment << name << ": " << t;
    img.comment(std::make_pair("distortion", comment.str()));

    image::io::write(img, finFN, image::io::SampleBitDepth_8);
}

#endif // _DISTORTIONS_HPP_

