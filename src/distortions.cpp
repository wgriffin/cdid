#include "distortions.hpp"
#include "util.hpp"
#include "vonmises.hpp"

#include <stdexcept>
#include <tuple>

static std::random_device rd;
std::mt19937 distortion::gen(rd());
cl_int2 distortion::nthreads;
cl::CommandQueue distortion::queue;
cl::Program distortion::color_space, distortion::image_stats, distortion::correlation, distortion::distortions;

void distortion::undistort(distortion& task) {
    task.is_undistorted = true;

    std::ifstream in{ task.srcFN.c_str(), std::ios::binary };
    if (!in) throw std::runtime_error{ "cannot open " + task.srcFN };

    std::ofstream out{ task.finFN.c_str(), std::ios::binary };
    if (!out) throw std::runtime_error{ "cannot open " + task.finFN };

    in.seekg(0, in.end);
    auto size = in.tellg();
    in.seekg(0);

    char* buf = new char[size];
    in.read(buf, size);
    out.write(buf, size);
    delete[] buf;
}

void distortion::cd(int shift, distortion& task) {
    task.shift = shift;

    task.read_input();
    task.enqueue_convert("convert_rgb2lab", task.srcIMG, task.convIMG);
    cl::Event::waitForEvents(task.convert_event);

    auto red = resize(task.srcIMGhost, task.srcIMGhost.width()+6, task.srcIMGhost.height()+6);
    auto green = resize(red, red.width()+(1*shift), red.height()+(1*shift));
    auto blue =  resize(red, red.width()+(2*shift), red.height()+(2*shift));

    image::image dst(task.srcIMGhost.width(), task.srcIMGhost.height(), 1, 4);

    size_t const red_w = static_cast<size_t>(static_cast<float>(red.width()-dst.width())/2.0f);
    size_t const red_h = static_cast<size_t>(static_cast<float>(red.height()-dst.height())/2.0f);

    for (size_t y=red_h; y<dst.height()+red_h; ++y) {
        for (size_t x=red_w; x<dst.width()+red_w; ++x) {
            dst.pixel(0, x-red_w, y-red_h, 0) = red.pixel(0, x, y, 0);
        }
    }

    size_t const green_w = static_cast<size_t>(static_cast<float>(green.width()-dst.width())/2.0f);
    size_t const green_h = static_cast<size_t>(static_cast<float>(green.height()-dst.height())/2.0f);

    for (size_t y=green_h; y<dst.height()+green_h; ++y) {
        for (size_t x=green_w; x<dst.width()+green_w; ++x) {
            dst.pixel(1, x-green_w, y-green_h, 0) = green.pixel(1, x, y, 0);
        }
    }

    size_t const blue_w = static_cast<size_t>(static_cast<float>(blue.width()-dst.width())/2.0f);
    size_t const blue_h = static_cast<size_t>(static_cast<float>(blue.height()-dst.height())/2.0f);

    for (size_t y=blue_h; y<dst.height()+blue_h; ++y) {
        for (size_t x=blue_w; x<dst.width()+blue_w; ++x) {
            dst.pixel(2, x-blue_w, y-blue_h, 0) = blue.pixel(2, x, y, 0);
        }
    }


    auto context = task.queue.getInfo<CL_QUEUE_CONTEXT>();
    auto dstIMG = dst.toCL2D(context);
    task.enqueue_convert("convert_rgb2lab", dstIMG, task.dstIMG);
    cl::Event::waitForEvents(task.convert_event);

    auto srcLAB = image::image(task.queue, task.convIMG);
    auto dstLAB = image::image(task.queue, task.dstIMG);

    for (size_t i=0; i<dstLAB.npixels(); ++i) dstLAB.pixel(0, i) = dstLAB.pixel(0, i);

    task.dstIMG = dstLAB.toCL2D(context);
    task.enqueue_convert("convert_lab2rgb", task.dstIMG, task.finIMG);
    task.complete_task(&task.convert_event, "shift", task.shift);
}

void distortion::dsab(int blocksize, distortion& task) {
    task.blocksize = blocksize;

    task.read_input();
    task.enqueue_convert("convert_rgb2lab", task.srcIMG, task.convIMG);
    cl::Event::waitForEvents(task.convert_event);

    image::image lab(task.queue, task.convIMG);

    auto ds = resize(lab, lab.width()/blocksize, lab.height()/blocksize, 0.0f, 0.0f, "box");
    auto rs = resize(ds, lab.width(), lab.height(), 0.0f, 0.0f, "box");

    image::image dst(lab);

    for (size_t i=0; i<dst.npixels(); ++i) {
        dst.pixel(1, i) = rs.pixel(1, i);
        dst.pixel(2, i) = rs.pixel(2, i);
    }

    auto context = task.queue.getInfo<CL_QUEUE_CONTEXT>();
    task.dstIMG = dst.toCL2D(context);

    task.enqueue_convert("convert_lab2rgb", task.dstIMG, task.finIMG);
    task.complete_task(&task.convert_event, "blocksize", task.blocksize);
}

void distortion::hr(float kappa, distortion& task) {
    task.kappa = kappa;

    task.read_input();
    task.enqueue_convert("convert_rgb2hsv", task.srcIMG, task.convIMG);

    auto hsv = image::image(task.queue, task.convIMG);
    for (size_t i=0; i<hsv.npixels(); ++i) {
        auto hue = deg2rad(hsv.pixel(0, i));
        vonmises_distribution<double> d{ hue, task.kappa };

        auto new_hue = d(task.gen);
        if (new_hue < 0) new_hue += 2*M_PI;
        if (new_hue > 2*M_PI) new_hue -= 2*M_PI;

        hsv.pixel(0, i) = rad2deg(new_hue);
    }

    auto context = task.queue.getInfo<CL_QUEUE_CONTEXT>();
    task.dstIMG = hsv.toCL2D(context);

    task.enqueue_convert("convert_hsv2rgb", task.dstIMG, task.finIMG);
    task.complete_task(&task.convert_event, "kappa", task.kappa);
}

void distortion::gbab(float sigma, distortion& task) {
    task.sigma = sigma;

    task.read_input();
    task.enqueue_convert("convert_rgb2lab", task.srcIMG, task.convIMG);

    int width;
    std::vector<float> filter;
    std::tie(width, filter) = gaussian_kernel_1d(task.sigma);

    cl::Context context = task.queue.getInfo<CL_QUEUE_CONTEXT>();
    cl_int flags = CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR;
    cl::Buffer filter_dev(context, flags, filter.size()*sizeof(float), filter.data());

    cl_uint4 filter_mask = {{ 0u, -1u, -1u, 0u }};
    cl::Sampler sampler(context, CL_TRUE, CL_ADDRESS_CLAMP, CL_FILTER_NEAREST);

    std::tie(task.dstIMG, task.distort_event[0]) = correlate2d(task.queue, task.convIMG, sampler,
        filter_dev, width, filter_mask, cl::Kernel(task.correlation, "correlate1d_x"),
        cl::Kernel(task.correlation, "correlate1d_y"), task.nthreads, &task.convert_event);

    task.enqueue_convert("convert_lab2rgb", task.dstIMG, task.finIMG, &task.distort_event);
    task.complete_task(&task.convert_event, "sigma", task.sigma);
}

void distortion::qab(int nbins, distortion& task) {
    task.nbins = nbins;

    task.read_input();
    task.enqueue_convert("convert_rgb2lab", task.srcIMG, task.convIMG);
    task.enqueue_min_max(&task.convert_event);

    task.queue.enqueueReadBuffer(task.min_max.first, CL_TRUE, 0, sizeof(task.min), &task.min,
                                 &task.convert_event);
    task.queue.enqueueReadBuffer(task.min_max.second, CL_TRUE, 0, sizeof(task.max), &task.max,
                                 &task.convert_event);

    cl::Context context = task.queue.getInfo<CL_QUEUE_CONTEXT>();
    cl_int flags = CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR;

    auto a_bins = linspace(task.min.s[1], task.max.s[1], task.nbins);
    cl::Buffer a_bins_dev(context, flags, a_bins.size()*sizeof(float), a_bins.data());
    auto b_bins = linspace(task.min.s[2], task.max.s[2], task.nbins);
    cl::Buffer b_bins_dev(context, flags, b_bins.size()*sizeof(float), b_bins.data());

    cl::Kernel kernel(task.distortions, "distort_quantize_ab");
    task.distort_event[0] = enqueue(task.queue, kernel, cl::NullRange, task.global, task.local, NULL,
                                    0, task.convIMG, a_bins_dev, b_bins_dev, task.nbins, task.dstIMG);

    task.enqueue_convert("convert_lab2rgb", task.dstIMG, task.finIMG, &task.distort_event);
    task.complete_task(&task.convert_event, "nbins", task.nbins);
}

distortion::distortion(std::string const& srcFN_, std::string const& finFN_)
    : srcFN(srcFN_), finFN(finFN_),
    convert_event(1), min_max_events(2), distort_event(1), is_undistorted(false) {
    origin[0] = origin[1] = origin[2] = 0;
}

void distortion::read_input() {
    auto context = queue.getInfo<CL_QUEUE_CONTEXT>();

    srcIMGhost = image::io::read(srcFN);
    srcIMGhost.to_linear(0, 3);
    if (srcIMGhost.ncomponents() != 4) srcIMGhost.resize_component_count(4);

    region[0] = srcIMGhost.width(); region[1] = srcIMGhost.height(); region[2] = 1;
    srcIMG = srcIMGhost.toCL2D(context);

    std::tie(global, local) = compute_global_local(srcIMG.getImageInfo<CL_IMAGE_WIDTH>(),
                                                   srcIMG.getImageInfo<CL_IMAGE_HEIGHT>(), nthreads);

    cl::ImageFormat fmt(srcIMG.getImageInfo<CL_IMAGE_FORMAT>().image_channel_order,
                        srcIMG.getImageInfo<CL_IMAGE_FORMAT>().image_channel_data_type);

    convIMG = cl::Image2D(context, CL_MEM_READ_WRITE, fmt, srcIMGhost.width(), srcIMGhost.height());
    dstIMG = cl::Image2D(context, CL_MEM_READ_WRITE, fmt, srcIMGhost.width(), srcIMGhost.height());
    finIMG = cl::Image2D(context, CL_MEM_READ_WRITE, fmt, srcIMGhost.width(), srcIMGhost.height());
}

void distortion::enqueue_convert(char const* kernel_name, cl::Image2D& in, cl::Image2D& out,
                             VECTOR_CLASS<cl::Event> *wait_events) {
    cl::Kernel kernel(color_space, kernel_name);
    convert_event[0] = enqueue(queue, kernel, cl::NullRange, global, local, wait_events, 0, in, out);
}

void distortion::enqueue_min_max(VECTOR_CLASS<cl::Event> *wait_events) {
    std::tie(min_max.first, min_max_events[0]) =
        compute_reduction(queue, convIMG, sizeof(cl_float4),
            { image_stats, "compute_min_partial" }, { image_stats, "compute_min_complete" },
            nthreads, wait_events);

    std::tie(min_max.second, min_max_events[1]) =
        compute_reduction(queue, convIMG, sizeof(cl_float4),
            { image_stats, "compute_max_partial" }, { image_stats, "compute_max_complete" },
            nthreads, wait_events);
}

