#ifndef _VONMISES_HPP_
#define _VONMISES_HPP_

#include <random>

/**
 * Random number distribution that produces floating-point values according to
 * a von Mises distribution, which is described by the following probability
 * density function:
 * f(x|u,k) = e^[k cos(x-u)]/[2 pi I_0(k)]
 * where I_0 is the modified Bessel function of order 0.
 * @see https://en.wikipedia.org/wiki/Von_Mises_distribution
 * @see https://github.com/numpy/numpy/blob/4cbced22a8306d7214a4e18d12e651c034143922/numpy/random/mtrand/distributions.c
 */
template <class RealType = double>
class vonmises_distribution {
public:
    using result_type = RealType;

    struct param_type {
        result_type mu;
        result_type kappa;
        typename std::uniform_real_distribution<result_type>::param_type state;

        param_type(result_type m, result_type k, decltype(state) s) : mu{ m }, kappa{ k }, state{ s } {}
    };

    explicit vonmises_distribution(result_type mu, result_type kappa)
        : _u{ 0.0, 1.0 }, _param{ mu, kappa, _u.param() } {}
    explicit vonmises_distribution(param_type const& param)
        : _u{ param.state }, _param{ param } {}

    template <class URNG>
    result_type operator()(URNG& g) { return operator()(g, _param); }

    template <class URNG>
    result_type operator()(URNG& g, param_type const& param);

    param_type param() const { return _param; }
    void param(param_type const& param) { _param = param; }

    result_type mu() const { return _param.mu; }
    result_type kappa() const { return _param.kappa; }

    void reset() {
        _u.reset();
    }

private:
    std::uniform_real_distribution<result_type> _u;
    param_type _param;
};


#include <cmath>

template <class RealType> template <class URNG>
typename vonmises_distribution<RealType>::result_type
vonmises_distribution<RealType>::operator()(URNG& g, param_type const& param) {
    if (param.kappa < 1.0e-8) {
        return M_PI*(2.0*_u(g) - 1.0);
    } else {
        result_type s;
        if (param.kappa < 1.0e-5) {
            s = (1.0/param.kappa + param.kappa);
        } else {
            result_type r = 1.0 + std::sqrt(1.0 + 4.0*param.kappa*param.kappa);
            result_type rho = (r - std::sqrt(2.0*r)) / (2.0*param.kappa);
            s = (1.0 + rho*rho)/(2.0*rho);
        }

        result_type W;
        for (;;) {
            result_type U = _u(g);
            result_type Z = std::cos(M_PI*U);
            W = (1.0 + s*Z)/(s + Z);
            result_type Y = param.kappa*(s - W);
            result_type V = _u(g);

            if ((Y*(2.0-Y) - V >= 0.0) || (std::log(Y/V) + 1.0 - Y >= 0.0)) break;
        }

        result_type result = std::acos(W);
        result_type U = _u(g);
        if (U < 0.5) result = -result;
        result += param.mu;

        result_type mod = std::fmod(std::fabs(result) + M_PI, 2.0*M_PI) - M_PI;
        if (result < 0.0) mod *= -1.0;

        return mod;
    }
}


template <class RealType>
bool operator==(vonmises_distribution<RealType> const& lhs,
                vonmises_distribution<RealType> const& rhs) {
    return
        lhs._param.mu == rhs._param.mu &&
        lhs._param.kappa = rhs._param.kappa &&
        lhs._u == rhs._u;
}

template <class RealType>
bool operator!=(vonmises_distribution<RealType> const& lhs,
                vonmises_distribution<RealType> const& rhs) {
    return !(lhs == rhs);
}


#include <cassert>
#include <iomanip>
#include <ostream>

template <class charT, class traits, class RealType>
std::basic_ostream<charT, traits>& operator<<(std::basic_ostream<charT, traits>& os,
                                              vonmises_distribution<RealType> const& distr) {
    assert(true);
}

template <class charT, class traits, class RealType>
std::basic_ostream<charT, traits>& operator>>(std::basic_ostream<charT, traits>& os,
                                              vonmises_distribution<RealType>& distr) {
    assert(true);
}

#endif // _VONMISES_HPP_

