#include "cl_util.hpp"

#include <cmath>
#include <fstream>
#include <iomanip>
#include <ostream>
#include <sstream>
#include <stdexcept>

cl::Context init_cl(cl_int deviceType) {
    VECTOR_CLASS<cl::Platform> platforms;
    cl::Platform::get(&platforms);
    if (platforms.empty()) {
        throw std::runtime_error("no OpenCL platforms");
    }

    cl_context_properties properties[] = {
        CL_CONTEXT_PLATFORM, (cl_context_properties)(platforms[0])(),
        0
    };

    cl::Context context(deviceType, properties);
    if (context.getInfo<CL_CONTEXT_DEVICES>().empty()) {
        throw std::runtime_error("no OpenCL devices");
    }

    return context;
}

cl::Program::Sources read_source(std::string const& filename) {
    std::ifstream is(filename.c_str(), std::ios::binary);
    if (!is) throw std::runtime_error("cannot open "+filename);

    is.seekg(0, std::ios_base::end);
    size_t size = is.tellg();
    is.seekg(0, std::ios_base::beg);
    if (!is) throw std::runtime_error("error reading "+filename);

    char* data = new char[size];
    is.read(data, size);
    if (!is) throw std::runtime_error("error reading "+filename);
    is.close();

    cl::Program::Sources source(1, std::make_pair(data, size));
    return source;
}

cl::Program build_program(cl::Context& context, std::string const& filename, std::string const& flags) {
    cl::Program program;

    try {

        program = cl::Program(context, read_source(filename));
        program.build(context.getInfo<CL_CONTEXT_DEVICES>(), flags.c_str());

    } catch (cl::Error const& err) {
        std::ostringstream oss;
        oss << "error creating '" << filename << "': (" << err.what() << "): "
            << error_string(err.err()) << " (" << err.err() << ")";

        if (err.err() == CL_BUILD_PROGRAM_FAILURE) {
            VECTOR_CLASS<cl::Device> devices = context.getInfo<CL_CONTEXT_DEVICES>();

            cl::STRING_CLASS log = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(devices[0]);
            if (!log.empty()) oss << "\n" << filename << " build log:\n" << log;
        }

        throw std::runtime_error(oss.str());
    }

    return program;
}

std::string error_string(cl_int error) {
    switch(error) {
    case CL_SUCCESS: return "SUCCESS";
    case CL_DEVICE_NOT_FOUND: return "DEVICE_NOT_FOUND";
    case CL_DEVICE_NOT_AVAILABLE: return "DEVICE_NOT_AVAILABLE";
    case CL_COMPILER_NOT_AVAILABLE: return "COMPILER_NOT_AVAILABLE";
    case CL_MEM_OBJECT_ALLOCATION_FAILURE: return "MEM_OBJECT_ALLOCATION_FAILURE";
    case CL_OUT_OF_RESOURCES: return "OUT_OF_RESOURCES";
    case CL_OUT_OF_HOST_MEMORY: return "OUT_OF_HOST_MEMORY";
    case CL_PROFILING_INFO_NOT_AVAILABLE: return "PROFILING_INFO_NOT_AVAILABLE";
    case CL_MEM_COPY_OVERLAP: return "MEM_COPY_OVERLAP";
    case CL_IMAGE_FORMAT_MISMATCH: return "IMAGE_FORMAT_MISMATCH";
    case CL_IMAGE_FORMAT_NOT_SUPPORTED: return "IMAGE_FORMAT_NOT_SUPPORTED";
    case CL_BUILD_PROGRAM_FAILURE: return "BUILD_PROGRAM_FAILURE";
    case CL_MAP_FAILURE: return "MAP_FAILURE";
    case CL_MISALIGNED_SUB_BUFFER_OFFSET: return "MISALIGNED_SUB_BUFFER_OFFSET";
    case CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
    case CL_INVALID_VALUE: return "INVALID_VALUE";
    case CL_INVALID_DEVICE_TYPE: return "INVALID_DEVICE_TYPE";
    case CL_INVALID_PLATFORM: return "INVALID_PLATFORM";
    case CL_INVALID_DEVICE: return "INVALID_DEVICE";
    case CL_INVALID_CONTEXT: return "INVALID_CONTEXT";
    case CL_INVALID_QUEUE_PROPERTIES: return "INVALID_QUEUE_PROPERTIES";
    case CL_INVALID_COMMAND_QUEUE: return "INVALID_COMMAND_QUEUE";
    case CL_INVALID_HOST_PTR: return "INVALID_HOST_PTR";
    case CL_INVALID_MEM_OBJECT: return "INVALID_MEM_OBJECT";
    case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR: return "INVALID_IMAGE_FORMAT_DESCRIPTOR";
    case CL_INVALID_IMAGE_SIZE: return "INVALID_IMAGE_SIZE";
    case CL_INVALID_SAMPLER: return "INVALID_SAMPLER";
    case CL_INVALID_BINARY: return "INVALID_BINARY";
    case CL_INVALID_BUILD_OPTIONS: return "INVALID_BUILD_OPTIONS";
    case CL_INVALID_PROGRAM: return "INVALID_PROGRAM";
    case CL_INVALID_PROGRAM_EXECUTABLE: return "INVALID_PROGRAM_EXECUTABLE";
    case CL_INVALID_KERNEL_NAME: return "INVALID_KERNEL_NAME";
    case CL_INVALID_KERNEL_DEFINITION: return "INVALID_KERNEL_DEFINITION";
    case CL_INVALID_KERNEL: return "INVALID_KERNEL";
    case CL_INVALID_ARG_INDEX: return "INVALID_ARG_INDEX";
    case CL_INVALID_ARG_VALUE: return "INVALID_ARG_VALUE";
    case CL_INVALID_ARG_SIZE: return "INVALID_ARG_SIZE";
    case CL_INVALID_KERNEL_ARGS: return "INVALID_KERNEL_ARGS";
    case CL_INVALID_WORK_DIMENSION: return "INVALID_WORK_DIMENSION";
    case CL_INVALID_WORK_GROUP_SIZE: return "INVALID_WORK_GROUP_SIZE";
    case CL_INVALID_WORK_ITEM_SIZE: return "INVALID_WORK_ITEM_SIZE";
    case CL_INVALID_GLOBAL_OFFSET: return "INVALID_GLOBAL_OFFSET";
    case CL_INVALID_EVENT_WAIT_LIST: return "INVALID_EVENT_WAIT_LIST";
    case CL_INVALID_EVENT: return "INVALID_EVENT";
    case CL_INVALID_OPERATION: return "INVALID_OPERATION";
    case CL_INVALID_GL_OBJECT: return "INVALID_GL_OBJECT";
    case CL_INVALID_BUFFER_SIZE: return "INVALID_BUFFER_SIZE";
    case CL_INVALID_MIP_LEVEL: return "INVALID_MIP_LEVEL";
    case CL_INVALID_GLOBAL_WORK_SIZE: return "INVALID_GLOBAL_WORK_SIZE";
    case CL_INVALID_PROPERTY: return "INVALID_PROPERTY";
    default: return "UNKNOWN";
    }
}

std::pair<cl::NDRange, cl::NDRange> compute_global_local(size_t width, size_t height, cl_int2 nthreads) {
    return std::make_pair(
                          cl::NDRange(std::ceil(static_cast<float>(width)/nthreads.s[0])*nthreads.s[0],
                                      std::ceil(static_cast<float>(height)/nthreads.s[1])*nthreads.s[1]),
                          cl::NDRange(nthreads.s[0], nthreads.s[1]));
}

std::pair<cl::Buffer, cl::Event>
compute_reduction(cl::CommandQueue& queue, cl::Image2D& image, size_t local_size,
                  cl::Kernel partial_kernel, cl::Kernel complete_kernel,
                  cl_int2 nthreads, VECTOR_CLASS<cl::Event>* wait_events) {
    cl::Context context = queue.getInfo<CL_QUEUE_CONTEXT>();

    size_t const width = image.getImageInfo<CL_IMAGE_WIDTH>();
    size_t const height = image.getImageInfo<CL_IMAGE_HEIGHT>();

    cl::ImageFormat const fmt(image.getImageInfo<CL_IMAGE_FORMAT>().image_channel_order,
                              image.getImageInfo<CL_IMAGE_FORMAT>().image_channel_data_type);
    cl::Image2D ping(context, CL_MEM_READ_WRITE, fmt, width, height);
    cl::Image2D pong(context, CL_MEM_READ_WRITE, fmt, width, height);


    auto glp = compute_global_local(width, height, nthreads);
    cl::NDRange global = glp.first;
    cl::NDRange local = glp.second;

    VECTOR_CLASS<cl::Event> partial_wait_event(1);

    int step = 0;
    bool quit = false;
    while (!quit) {
        if (step == 0) {
            partial_kernel.setArg(0, image);
            partial_kernel.setArg(2, pong);
        } else if (step%2 == 0) {
            partial_kernel.setArg(0, ping);
            partial_kernel.setArg(2, pong);
        } else {
            partial_kernel.setArg(0, pong);
            partial_kernel.setArg(2, ping);
        }

        partial_kernel.setArg(1, cl::__local(local_size*nthreads.s[0]*nthreads.s[1]));

        cl::Event partial_event;
        if (step == 0) {
            queue.enqueueNDRangeKernel(partial_kernel, cl::NullRange, global, local,
                                       wait_events, &partial_event);
        } else {
            queue.enqueueNDRangeKernel(partial_kernel, cl::NullRange, global, local,
                                       &partial_wait_event, &partial_event);
        }

        global = cl::NDRange(static_cast<size_t const*>(global)[0]/nthreads.s[0],
                             static_cast<size_t const*>(global)[0]/nthreads.s[0]);

        partial_wait_event[0] = partial_event;
        step += 1;

        quit = ((static_cast<size_t const*>(global)[0] < static_cast<size_t const*>(local)[0]) &&
                (static_cast<size_t const*>(global)[1] < static_cast<size_t const*>(local)[1]));
    }


    local = global;
    cl::Buffer buffer(context, CL_MEM_WRITE_ONLY, local_size);


    if (step%2 == 0) {
        complete_kernel.setArg(0, ping);
    } else {
        complete_kernel.setArg(0, pong);
    }

    complete_kernel.setArg(1, cl::__local(local_size*nthreads.s[0]*nthreads.s[1]));
    complete_kernel.setArg(2, buffer);


    cl::Event complete_event;
    queue.enqueueNDRangeKernel(complete_kernel, cl::NullRange, global, local,
                               &partial_wait_event, &complete_event);


    return std::make_pair(buffer, complete_event);
}

std::pair<cl::Image2D, cl::Event>
correlate2d(cl::CommandQueue& queue, cl::Image2D& image, cl::Sampler sampler,
            cl::Buffer& filter, cl_int2 filter_width, cl_uint4 filter_mask,
            cl::Kernel kernel_xy, cl_int2 nthreads, VECTOR_CLASS<cl::Event>* wait_events) {
    cl::Context context = queue.getInfo<CL_QUEUE_CONTEXT>();

    size_t const width = image.getImageInfo<CL_IMAGE_WIDTH>();
    size_t const height = image.getImageInfo<CL_IMAGE_HEIGHT>();

    cl::ImageFormat const fmt(image.getImageInfo<CL_IMAGE_FORMAT>().image_channel_order,
                              image.getImageInfo<CL_IMAGE_FORMAT>().image_channel_data_type);
    cl::Image2D final(context, CL_MEM_READ_WRITE, fmt, width, height);


    auto glp = compute_global_local(width, height, nthreads);


    cl::Event final_event = enqueue(queue, kernel_xy, cl::NullRange, glp.first, glp.second, wait_events,
                                    0, image, sampler, filter, filter_width, filter_mask, final);

    return std::make_pair(final, final_event);
}

std::pair<cl::Image2D, cl::Event>
correlate2d(cl::CommandQueue& queue, cl::Image2D& image, cl::Sampler sampler,
            cl::Buffer& filter, int filter_width, cl_uint4 filter_mask,
            cl::Kernel kernel_x, cl::Kernel kernel_y,
            cl_int2 nthreads, VECTOR_CLASS<cl::Event>* wait_events) {
    cl::Context context = queue.getInfo<CL_QUEUE_CONTEXT>();

    size_t const width = image.getImageInfo<CL_IMAGE_WIDTH>();
    size_t const height = image.getImageInfo<CL_IMAGE_HEIGHT>();

    cl::ImageFormat const fmt(image.getImageInfo<CL_IMAGE_FORMAT>().image_channel_order,
                              image.getImageInfo<CL_IMAGE_FORMAT>().image_channel_data_type);
    cl::Image2D inter(context, CL_MEM_READ_WRITE, fmt, width, height);
    cl::Image2D final(context, CL_MEM_READ_WRITE, fmt, width, height);


    auto glp = compute_global_local(width, height, nthreads);


    VECTOR_CLASS<cl::Event> inter_event(1);
    inter_event[0] = enqueue(queue, kernel_x, cl::NullRange, glp.first, glp.second, wait_events,
                             0, image, sampler, filter, filter_width, filter_mask, inter);

    cl::Event final_event = enqueue(queue, kernel_y, cl::NullRange, glp.first, glp.second, &inter_event,
                                    0, inter, sampler, filter, filter_width, filter_mask, final);

    return std::make_pair(final, final_event);
}

std::ostream& operator<<(std::ostream& os, cl_float4 const& v) {
    return os << "(" << std::scientific << std::showpos
        << std::setprecision(8) << v.s[0] << ", "
        << std::setprecision(8) << v.s[1] << ", "
        << std::setprecision(8) << v.s[2] << ", "
        << std::setprecision(8) << v.s[3] << ")";
}

std::ostream& operator<<(std::ostream& os, cl::Event const& e) {
    cl_ulong start = e.getProfilingInfo<CL_PROFILING_COMMAND_START>();
    cl_ulong end = e.getProfilingInfo<CL_PROFILING_COMMAND_END>();
    return os << (end-start)/1e6 << "ms";
}

