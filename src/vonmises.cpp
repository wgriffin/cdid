#include "vonmises.hpp"

#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <iterator>
#include <vector>

#include <getopt.h>

int main(int argc, char* argv[]) {
    double kappa = 4;
    double mu = 0;
    int nsamples = 1000;

    char const* sopts = "hk:m:n:";

    static struct option lopts[] = {
        { "help", no_argument, NULL, 'h' },
        { "kappa", required_argument, NULL, 'k' },
        { "mu", required_argument, NULL, 'm' },
        { "num-samples", required_argument, NULL, 'n' },
        { NULL, 0, NULL, 0 }
    };

    int sopt, lopt_idx;
    while ((sopt = getopt_long(argc, argv, sopts, lopts, &lopt_idx)) != -1) {
        switch(sopt) {
        case 'k':
            kappa = std::stod(optarg);
            break;

        case 'm':
            mu = std::stod(optarg);
            break;

        case 'n':
            nsamples = std::stoi(optarg);
            break;

        case 'h':
            std::cerr
                << "usage: " << argv[0] << " [options]\n"
                << "    -k,--kappa        kappa for the von Mises distribution (default: "
                    << kappa << ")\n"
                << "    -m,--mu           mu for the von Mises distribution (default: "
                    << mu << ")\n"
                << "    -n,--num-samples  number of samples to generate (default: "
                    << nsamples << ")\n"
            ;
            exit(EXIT_SUCCESS);

        case '?':
        default:
            exit(EXIT_FAILURE);
        }
    }



    auto d = std::default_random_engine{};
    auto vm = vonmises_distribution<double>{ mu, kappa };

    auto s = std::vector<double>(nsamples);
    std::generate_n(s.begin(), s.size(), std::bind(vm, d));

    std::copy(s.begin(), s.end(), std::ostream_iterator<double>(std::cout, " "));
}

