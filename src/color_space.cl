__constant sampler_t gSampler =
    CLK_NORMALIZED_COORDS_FALSE|CLK_ADDRESS_NONE|CLK_FILTER_NEAREST;

#include "color_space.clh"

__kernel void convert_rgb2rgb(__read_only image2d_t src, __write_only image2d_t dst) {
    int2 const xy = (int2)(get_global_id(0), get_global_id(1));
    if (xy.x >= get_image_width(src) || xy.y >= get_image_height(src)) return;
    float4 const rgba = read_imagef(src, gSampler, xy);
    write_imagef(dst, xy, rgba);
}

__kernel void convert_lab2lab(__read_only image2d_t src, __write_only image2d_t dst) {
    int2 const xy = (int2)(get_global_id(0), get_global_id(1));
    if (xy.x >= get_image_width(src) || xy.y >= get_image_height(src)) return;
    float4 const laba = read_imagef(src, gSampler, xy);
    write_imagef(dst, xy, laba);
}

__kernel void convert_rgb2xyz(__read_only image2d_t src, __write_only image2d_t dst) {
    int2 const xy = (int2)(get_global_id(0), get_global_id(1));
    if (xy.x >= get_image_width(src) || xy.y >= get_image_height(src)) return;
    float4 const rgba = read_imagef(src, gSampler, xy);
    float3 const xyz = rgb2xyz(rgba.s012);
    write_imagef(dst, xy, (float4)(xyz, rgba.s3));
}

__kernel void convert_xyz2rgb(__read_only image2d_t src, __write_only image2d_t dst) {
    int2 const xy = (int2)(get_global_id(0), get_global_id(1));
    if (xy.x >= get_image_width(src) || xy.y >= get_image_height(src)) return;
    float4 const xyza = read_imagef(src, gSampler, xy);
    float3 const rgb = xyz2rgb(xyza.s012);
    write_imagef(dst, xy, (float4)(rgb, xyza.s3));
}

__kernel void convert_rgb2lab(__read_only image2d_t src, __write_only image2d_t dst) {
    int2 const xy = (int2)(get_global_id(0), get_global_id(1));
    if (xy.x >= get_image_width(src) || xy.y >= get_image_height(src)) return;
    float4 const rgba = read_imagef(src, gSampler, xy);
    float3 const xyz = rgb2xyz(rgba.s012);
    float3 const lab = xyz2lab(xyz);
    write_imagef(dst, xy, (float4)(lab, rgba.s3));
}

__kernel void convert_lab2rgb(__read_only image2d_t src, __write_only image2d_t dst) {
    int2 const xy = (int2)(get_global_id(0), get_global_id(1));
    if (xy.x >= get_image_width(src) || xy.y >= get_image_height(src)) return;
    float4 const laba = read_imagef(src, gSampler, xy);
    float3 const xyz = lab2xyz(laba.s012);
    float3 const rgb = xyz2rgb(xyz);
    write_imagef(dst, xy, (float4)(rgb, laba.s3));
}

__kernel void convert_lab2lch(__read_only image2d_t src, __write_only image2d_t dst) {
    int2 const xy = (int2)(get_global_id(0), get_global_id(1));
    if (xy.x >= get_image_width(src) || xy.y >= get_image_height(src)) return;
    float4 const laba = read_imagef(src, gSampler, xy);
    float3 const lch = lab2lch(laba.s012);
    write_imagef(dst, xy, (float4)(lch, laba.s3));
}

__kernel void convert_lch2lab(__read_only image2d_t src, __write_only image2d_t dst) {
    int2 const xy = (int2)(get_global_id(0), get_global_id(1));
    if (xy.x >= get_image_width(src) || xy.y >= get_image_height(src)) return;
    float4 const lcha = read_imagef(src, gSampler, xy);
    float3 const lab = lch2lab(lcha.s012);
    write_imagef(dst, xy, (float4)(lab, lcha.s3));
}

__kernel void convert_rgb2hsv(__read_only image2d_t src, __write_only image2d_t dst) {
    int2 const xy = (int2)(get_global_id(0), get_global_id(1));
    if (xy.x >= get_image_width(src) || xy.y >= get_image_height(src)) return;
    float4 const rgba = read_imagef(src, gSampler, xy);
    float3 const hsv = rgb2hsv(rgba.s012);
    write_imagef(dst, xy, (float4)(hsv, rgba.s3));
}

__kernel void convert_hsv2rgb(__read_only image2d_t src, __write_only image2d_t dst) {
    int2 const xy = (int2)(get_global_id(0), get_global_id(1));
    if (xy.x >= get_image_width(src) || xy.y >= get_image_height(src)) return;
    float4 const hsva = read_imagef(src, gSampler, xy);
    float3 const rgb = hsv2rgb(hsva.s012);
    write_imagef(dst, xy, (float4)(rgb, hsva.s3));
}

