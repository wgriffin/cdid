#include "distortions.hpp"
#include "util.hpp"

#include <algorithm>
#include <cerrno>
#include <cstdlib>
#include <iostream>
#include <random>

#include <getopt.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

template <typename G>
std::function<bool(int)> gen_id(G g, unsigned int td, float pd) {
    auto nd = static_cast<size_t>(static_cast<float>(td)*pd);
    auto dd = std::uniform_int_distribution<int>{ 0, static_cast<int>(td-1) };

    std::vector<int> dv;
    while (dv.size() < nd) {
        auto v = dd(g);
        if (std::binary_search(std::begin(dv), std::end(dv), v)) continue;
        dv.push_back(v);
        std::sort(std::begin(dv), std::end(dv));
    }

    return [dv](int i){ return std::binary_search(std::begin(dv), std::end(dv), i); };
}

int main(int argc, char* argv[]) {
    std::string kernel_dir = "./";

    bool do_all = false;
    float percent_distorted = 0.9f;

    int do_cd = 0;
    unsigned int cd_ndist = 50;
    std::string cd_outdir = "cd/";

    int do_dsab = 0;
    unsigned int dsab_ndist = 100;
    std::string dsab_outdir = "dsab/";

    int do_gbab = 0;
    unsigned int gbab_ndist = 125;
    std::string gbab_outdir = "gbab/";

    int do_hr = 0;
    unsigned int hr_ndist = 125;
    std::string hr_outdir = "hr/";

    int do_qab = 0;
    unsigned int qab_ndist = 100;
    std::string qab_outdir = "qab/";

    char const* sopts = "ahk:p:";

    static struct option lopts[] = {
        { "all", no_argument, NULL, 'a' },
        { "help", no_argument, NULL, 'h' },
        { "kernel-dir", required_argument, NULL, 'k' },
        { "percent-distorted", required_argument, NULL, 'p' },
        { "cd", no_argument, &do_cd, 1 },
        { "cd-ndist", required_argument, NULL, 0 },
        { "cd-outdir", required_argument, NULL, 0 },
        { "dsab", no_argument, &do_dsab, 1 },
        { "dsab-ndist", required_argument, NULL, 0 },
        { "dsab-outdir", required_argument, NULL, 0 },
        { "gbab", no_argument, &do_gbab, 1 },
        { "gbab_ndist", required_argument, NULL, 0 },
        { "gbab-outdir", required_argument, NULL, 0 },
        { "hr", no_argument, &do_hr, 1 },
        { "hr-ndist", required_argument, NULL, 0 },
        { "hr-outdir", required_argument, NULL, 0 },
        { "qab", no_argument, &do_qab, 1 },
        { "qab-ndist", required_argument, NULL, 0 },
        { "qab-outdir", required_argument, NULL, 0 },
        { NULL, 0, NULL, 0 }
    };

    int sopt, lopt_idx;
    while ((sopt = getopt_long(argc, argv, sopts, lopts, &lopt_idx)) != -1) {
        switch(sopt) {
        case 0:
            if (strcmp(lopts[lopt_idx].name, "cd-outdir") == 0) {
                cd_outdir = optarg;
                if (cd_outdir[cd_outdir.size()-1] != '/') cd_outdir += "/";
            }

            if (strcmp(lopts[lopt_idx].name, "cd-ndist") == 0) {
                if (!safe_stoui(optarg, &cd_ndist)) {
                    std::cerr << "invalid value for --cd-ndist '" << optarg << "': "
                        << strerror(errno) << "\n";
                    exit(EXIT_FAILURE);
                }
            }

            if (strcmp(lopts[lopt_idx].name, "dsab-outdir") == 0) {
                dsab_outdir = optarg;
                if (dsab_outdir[dsab_outdir.size()-1] != '/') dsab_outdir += "/";
            }

            if (strcmp(lopts[lopt_idx].name, "dsab-ndist") == 0) {
                if (!safe_stoui(optarg, &dsab_ndist)) {
                    std::cerr << "invalid value for --dsab-ndist '" << optarg << "': "
                        << strerror(errno) << "\n";
                    exit(EXIT_FAILURE);
                }
            }

            if (strcmp(lopts[lopt_idx].name, "gbab-outdir") == 0) {
                gbab_outdir = optarg;
                if (gbab_outdir[gbab_outdir.size()-1] != '/') gbab_outdir += "/";
            }

            if (strcmp(lopts[lopt_idx].name, "gbab-ndist") == 0) {
                if (!safe_stoui(optarg, &gbab_ndist)) {
                    std::cerr << "invalid value for --gbab-ndist '" << optarg << "': "
                        << strerror(errno) << "\n";
                    exit(EXIT_FAILURE);
                }
            }

            if (strcmp(lopts[lopt_idx].name, "hr-outdir") == 0) {
                hr_outdir = optarg;
                if (hr_outdir[hr_outdir.size()-1] != '/') hr_outdir += "/";
            }

            if (strcmp(lopts[lopt_idx].name, "hr-ndist") == 0) {
                if (!safe_stoui(optarg, &hr_ndist)) {
                    std::cerr << "invalid value for --hr-ndist '" << optarg << "': "
                        << strerror(errno) << "\n";
                    exit(EXIT_FAILURE);
                }
            }

            if (strcmp(lopts[lopt_idx].name, "qab-outdir") == 0) {
                qab_outdir = optarg;
                if (qab_outdir[qab_outdir.size()-1] != '/') qab_outdir += "/";
            }

            if (strcmp(lopts[lopt_idx].name, "qab-ndist") == 0) {
                if (!safe_stoui(optarg, &qab_ndist)) {
                    std::cerr << "invalid value for --qab-ndist '" << optarg << "': "
                        << strerror(errno) << "\n";
                    exit(EXIT_FAILURE);
                }
            }

            break;

        case 'a':
            do_all = true;
            break;

        case 'k':
            kernel_dir = optarg;
            if (kernel_dir[kernel_dir.size()-1] != '/') kernel_dir += "/";
            break;

        case 'p':
            if (!safe_stof(optarg, &percent_distorted)) {
                std::cerr << "invalid percent distorted: " << optarg << ": "
                    << strerror(errno) << "\n";
                exit(EXIT_FAILURE);
            }
            break;

        case 'h':
            std::cerr
                << "usage: " << argv[0] << " [options] input_glob(s)...\n"
                << "    --all                   generate all distortions\n"
                << "    -k,--kernel-dir         kernel source directory\n"
                << "    -p,--percent-distorted  percent of images that are distorted (default: 90%)\n"
                << "\n"
                << "  distortions\n"
                << "\n"
                << "    --cd                    chrominance distort in rgb\n"
                << "      --cd-ndist            number of cd distortions (default: " << cd_ndist << ")\n"
                << "      --cd-outdir           output directory for cd\n"
                << "\n"
                << "    --dsab                   downsample a/b channels\n"
                << "      --dsab-ndist           number of dsab distortions (default: " << dsab_ndist << ")\n"
                << "      --dsab-outdir          output directory for dsab\n"
                << "\n"
                << "    --gbab                  gaussian blur a/b channels\n"
                << "      --gbab-ndist          number of gbab distortions (default: " << gbab_ndist << ")\n"
                << "      --gbab-outdir         output directory for gbab\n"
                << "\n"
                << "    --hr                    hue rotate in hsv\n"
                << "      --hr-ndist            number of hr distortions (default: " << hr_ndist << ")\n"
                << "      --hr-outdir           output directory for hr\n"
                << "\n"
                << "    --qab                   quantize a/b channels\n"
                << "      --qab-ndist           number of qab distortions (default: " << qab_ndist << ")\n"
                << "      --qab-outdir          output directory for qab\n"
                << "\n"
                ;

            exit(EXIT_SUCCESS);

        case '?':
        default:
            exit(EXIT_FAILURE);
        }
    }


    if (optind >= argc) {
        std::cerr << "no input specified\n";
        exit(EXIT_FAILURE);
    }


    if (do_all) do_cd = do_dsab = do_gbab = do_hr = do_qab = 1;

    if (do_cd && (mkdir(cd_outdir.c_str(), S_IRWXU|S_IRWXG|S_IRWXO) < 0) && errno != EEXIST) {
        std::cerr << "unable to create output directory " << cd_outdir << ": "
            << strerror(errno) << "\n";
        exit(EXIT_FAILURE);
    }

    if (do_dsab && (mkdir(dsab_outdir.c_str(), S_IRWXU|S_IRWXG|S_IRWXO) < 0) && errno != EEXIST) {
        std::cerr << "unable to create output directory " << dsab_outdir << ": "
            << strerror(errno) << "\n";
        exit(EXIT_FAILURE);
    }

    if (do_gbab && (mkdir(gbab_outdir.c_str(), S_IRWXU|S_IRWXG|S_IRWXO) < 0) && errno != EEXIST) {
        std::cerr << "unable to create output directory " << gbab_outdir << ": "
            << strerror(errno) << "\n";
        exit(EXIT_FAILURE);
    }

    if (do_hr && (mkdir(hr_outdir.c_str(), S_IRWXU|S_IRWXG|S_IRWXO) < 0) && errno != EEXIST) {
        std::cerr << "unable to create output directory " << hr_outdir << ": "
            << strerror(errno) << "\n";
        exit(EXIT_FAILURE);
    }

    if (do_qab && (mkdir(qab_outdir.c_str(), S_IRWXU|S_IRWXG|S_IRWXO) < 0) && errno != EEXIST) {
        std::cerr << "unable to create output directory " << qab_outdir << ": "
            << strerror(errno) << "\n";
        exit(EXIT_FAILURE);
    }



    cl::Context context;
    try {

        context = init_cl(CL_DEVICE_TYPE_GPU);

    } catch (cl::Error const& err) {
        std::cerr << "error initializing OpenCL (" << err.what() << "): "
            << error_string(err.err()) << " (" << err.err() << ")\n";
        exit(EXIT_FAILURE);
    } catch (std::exception const& err) {
        std::cerr << "error initializing OpenCL: " << err.what() << "\n";
        exit(EXIT_FAILURE);
    }


    cl::Device device = context.getInfo<CL_CONTEXT_DEVICES>()[0];
    int queue_properties = 0;
    cl::CommandQueue queue(context, device, queue_properties);


    std::vector<std::string> refimgnames;
    for (int i=optind; i<argc; ++i) {
        if (!read_glob(argv[i], refimgnames)) {
            std::cerr << "error reading glob " << argv[i] << "\n";
            exit(EXIT_FAILURE);
        }
    }

    cl_int2 nthreads = {{ 8, 8 }};

    std::random_device rd;
    std::mt19937 g(rd());

    if (do_cd == 1) {
        std::cout << "generating " << cd_ndist << " distortions into " << cd_outdir << "\n";
        size_t nv = cd_ndist/3;

        std::vector<int> dv(cd_ndist);
        for (int i=0; i<2; ++i) std::fill_n(std::begin(dv) + (i*nv), nv, i+2);
        std::fill_n(std::begin(dv) + (2*nv), dv.size() - (2*nv), 2+2);

        //std::shuffle(std::begin(dv), std::end(dv), g);
        auto pg = [&dv](){ auto v = dv.back(); dv.pop_back(); return v; };

        auto distortions = distortion::distort(queue, nthreads, kernel_dir, refimgnames, cd_outdir,
            cd_ndist, gen_id(g, cd_ndist, percent_distorted), pg, distortion::cd);
        distortion::write(distortions, cd_outdir + "info.txt", &distortion::shift);
    }

    if (do_dsab == 1) {
        std::cout << "generating " << dsab_ndist << " distortions into " << dsab_outdir << "\n";
        size_t nv = dsab_ndist/4;

        std::vector<int> dv(dsab_ndist);
        for (int i=0; i<3; ++i) {
            std::fill_n(std::begin(dv) + (i*nv), nv, static_cast<int>(std::pow(2, i+1)));
        }
        std::fill_n(std::begin(dv) + (3*nv), dv.size() - (3*nv), static_cast<int>(std::pow(2, 3+1)));

        //std::shuffle(std::begin(dv), std::end(dv), g);
        auto pg = [&dv](){ auto v = dv.back(); dv.pop_back(); return v; };

        auto distortions = distortion::distort(queue, nthreads, kernel_dir, refimgnames, dsab_outdir,
            dsab_ndist, gen_id(g, dsab_ndist, percent_distorted), pg, distortion::dsab);
        distortion::write(distortions, dsab_outdir + "info.txt", &distortion::blocksize);
    }

    if (do_gbab == 1) {
        std::cout << "generating " << gbab_ndist << " distortions into " << gbab_outdir << "\n";
        auto pd = std::normal_distribution<float>{ 7.5, 2.5 };
        auto pg = [&g, &pd](){ return std::max(pd(g), 0.0f); };

        auto distortions = distortion::distort(queue, nthreads, kernel_dir, refimgnames, gbab_outdir,
            gbab_ndist, gen_id(g, gbab_ndist, percent_distorted), pg, distortion::gbab);
        distortion::write(distortions, gbab_outdir + "info.txt", &distortion::sigma);
    }

    if (do_hr == 1) {
        std::cout << "generating " << hr_ndist << " distortions into " << hr_outdir << "\n";
        auto pd = std::normal_distribution<float>{ 100, 64 };
        auto pg = [&g, &pd](){ return std::max(pd(g), 1.0f); };

        auto distortions = distortion::distort(queue, nthreads, kernel_dir, refimgnames, hr_outdir,
            hr_ndist, gen_id(g, hr_ndist, percent_distorted), pg, distortion::hr);
        distortion::write(distortions, hr_outdir + "info.txt", &distortion::kappa);
    }

    if (do_qab == 1) {
        std::cout << "generating " << qab_ndist << " distortions into " << qab_outdir << "\n";
        size_t nv = qab_ndist/4;

        std::vector<int> dv(qab_ndist);
        for (int i=0; i<3; ++i) std::fill_n(std::begin(dv) + (i*nv), nv, (i+2)*2);
        std::fill_n(std::begin(dv) + (3*nv), dv.size() - (3*nv), (3+2)*2);

        //std::shuffle(std::begin(dv), std::end(dv), g);
        auto pg = [&dv](){ auto v = dv.back(); dv.pop_back(); return v; };

        auto distortions = distortion::distort(queue, nthreads, kernel_dir, refimgnames, qab_outdir,
            qab_ndist, gen_id(g, qab_ndist, percent_distorted), pg, distortion::qab);
        distortion::write(distortions, qab_outdir + "info.txt", &distortion::nbins);
    }

    return EXIT_SUCCESS;
}

