__constant sampler_t gSampler =
    CLK_NORMALIZED_COORDS_FALSE|CLK_ADDRESS_NONE|CLK_FILTER_NEAREST;

int find_nearest(__global float* bins, uint nbins, float value) {
    int argmin = 0;
    float valmin = fabs(bins[0]-value);

    for (uint i=1; i<nbins; ++i) {
        float valchk = fabs(bins[i]-value);
        if (valchk < valmin) {
            valmin = valchk;
            argmin = i;
        }
    }

    return argmin;
}

__kernel void distort_quantize_ab(__read_only image2d_t src,
    __global float* a_bins, __global float* b_bins, uint const nbins, __write_only image2d_t dst) {
    int2 xy = (int2)(get_global_id(0), get_global_id(1));
    if (xy.x >= get_image_width(dst) || xy.y >= get_image_height(dst)) return;

    float4 const laba = read_imagef(src, gSampler, xy);

    int const a_bins_argmin = find_nearest(a_bins, nbins, laba.s1);
    int const b_bins_argmin = find_nearest(b_bins, nbins, laba.s2);
    float3 const qab = (float3)(laba.s0, a_bins[a_bins_argmin], b_bins[b_bins_argmin]);

    write_imagef(dst, xy, (float4)(qab, laba.s3));
}

__kernel void distort_quantize_h(__read_only image2d_t src, __global float* bins, uint const nbins,
                                 __write_only image2d_t dst) {
    int2 xy = (int2)(get_global_id(0), get_global_id(1));
    if (xy.x >= get_image_width(dst) || xy.y >= get_image_height(dst)) return;

    float4 const hsva = read_imagef(src, gSampler, xy);

    int const bins_argmin = find_nearest(bins, nbins, hsva.s0);
    float3 const qh = (float3)(bins[bins_argmin], hsva.s1, hsva.s2);

    write_imagef(dst, xy, (float4)(qh, hsva.s3));
}

__kernel void distort_noise_ab(__read_only image2d_t src, float const noise,
                               __write_only image2d_t dst) {
    int2 xy = (int2)(get_global_id(0), get_global_id(1));
    if (xy.x >= get_image_width(dst) || xy.y >= get_image_height(dst)) return;

    float4 const laba = read_imagef(src, gSampler, xy);
    laba.s12 = laba.s12+(float2)noise;

    write_imagef(dst, xy, laba);
}

