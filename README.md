# Chrominance Distortion Image Database

This is a database of images with chrominance-only distortions and the
associated subjective image quality ratings generated using Mechanical Turk.

Contact: Wesley Griffin <griffin5@cs.umbc.edu>

## Project Layout

There are two directories at the top-level:

- data: images and subjective evaluation scores
- src: source code to create the distortions

### data

There are five types of distortions:

- cd: Geometrically shift the G and B channels of the image by different amounts.
- dsab: Downsample the A and B channels of the image in the Lab color space.
- hr: Random hue rotations of the H channel of the image in the HSV "color space".
- gbab: Gaussian blur the A and B channels of the image in the Lab color space.
- qab: Quantize the A and B channels of the image in the Lab color space.

For each distortion, there is a subdirectory containing the distorted images.
The subdirectory also contains an info.txt file which maps the distorted image
to the corresponding reference image and specifies the value of the parameter
for each distortion. An asterisk for the value indicates an image where no
distortion was performed, i.e., it is a copy of the reference image. For each
distortion, 10% of the images are reference images.

There are a total of 500 distorted images and 24 reference images. The
reference images are from the [Kodak Lossless True Color Image
Suite](http://r0k.us/graphics/kodak/index.html) and are stored in the refimgs
subdirectory.

The data subdirectory has a meanDMOS.csv file which lists the mean DMOS for
each distorted image. The mean DMOS was generated from a set of subjective
evaluation experiments performed on Mechanical Turk and described in the
paper: Wesley Griffin & Marc Olano. _A Chrominance Distortion Image Database_.

##### cd

The cd subdirectory has 50 images where the G and B channels are geometrically
shifted by different amounts. The G channel is shifted by a scale factor and
the B channel is shifted by twice the scale factor. The scale factor is in the
value in the info.txt file and is selected from the set [2,3,4]. There are 5
reference check images.

##### dsab

The dsab subdirectory has 100 images where the A and B channels are
downsampled after converting the RGB reference image to the Lab color space.
The parameter in inth info.txt file is the blocksize used for downsampling and
is selected from the set [2,4,8,16]. There are 10 reference check images.

##### gbab

The gbab subdirectory has 125 images where the A and B channels have been
blurred using a gaussian kernel after converting the RGB reference image to
the Lab color space. The gaussian kernel has a mean of 0 and a sigma randomly
selected from the normal distribution with mean 7.5 and sigma 2.5 which is the
parameter in the info.txt file. There are 13 reference check images.

##### hr

The hr subdirectory has 125 images where the H channel is rotated by a random
angle after converting the RGB reference image to the HSV color space. The
angle rotation is selected from the [von Mises
distribution](https://en.wikipedia.org/wiki/Von_Mises_distribution) where the
kappa for the von Mises distribution is selected from the normal distribution
with mean 100 and sigma 64 and is the parameter in the info.txt file. There
are 13 reference check images.

##### qab

The qab subdirectory has 100 images where the A and B channels have been
quantized after converting the RGB reference image to the Lab color space. The
parameter in the info.txt file is the number of bins used for quantization.
The bins are selected from the set [4,6,8,10]. There are 10 reference check
images.

### src

Contains the source code used to create the distortions.

#### Build Instructions

An OpenCL installation is required.

    $ mkdir build && cd build
    $ cmake -DCMAKE_BUILD_TYPE=Release ../src
    $ make

**Updated**: 2015 Oct 12

